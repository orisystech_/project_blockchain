package com.hyperledgerDemo.controller;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.hyperledgerDemo.service.ProductService;
import com.hyperledgerDemo.service.StateAndOWnerService;
import com.hyperledgerDemo.service.changeOrderService;
import com.hyperledgerDemo.service.chaincode.invocation.InvokeChaincode;
import com.hyperledgerDemo.service.chaincode.invocation.InvokeQueryChaincode;

@Controller
public class WebController{
    
    @Autowired
    private ProductService product;

    @Autowired
    private StateAndOWnerService stateAndOwner;

    @Autowired
    private changeOrderService changeOrder;


    
    
    @RequestMapping("/queryByOwner")
    @ResponseBody
    public JsonArray queryProductByOwner(@RequestBody StateAndOWnerService stateAndOwner) {
        String response = InvokeQueryChaincode.queryByOwner(stateAndOwner.getOwner());
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(response);
        JsonArray responseJson = element.getAsJsonArray();
        return responseJson;
        
    } 

    @RequestMapping("/queryByState")
    @ResponseBody
    public JsonArray queryBystate(@RequestBody StateAndOWnerService stateAndOwner) {
        String response = InvokeQueryChaincode.queryByState(stateAndOwner.getState());
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(response);
        JsonArray responseJson = element.getAsJsonArray();
        return responseJson;
        
    } 

    @RequestMapping("/queryByStateAndOwner")
    @ResponseBody
    public JsonArray queryBystateAndOwner(@RequestBody StateAndOWnerService stateAndOwner) {
        String response = InvokeQueryChaincode.queryByStateAndOwner(stateAndOwner.getOwner(),stateAndOwner.getState());
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(response);
        JsonArray responseJson = element.getAsJsonArray();
        return responseJson;
        
    } 

    @RequestMapping("/orderProduct")
    @ResponseBody
    public String orderProduct(@RequestBody ProductService product) {
        String response = InvokeChaincode.orderProduct(product);
        return response;
        
    } 

    @RequestMapping("/changeProductOrdered")
    @ResponseBody
    public String changeProductOrdered(@RequestBody changeOrderService order) {
        String response = InvokeChaincode.changeProductOrdered(order.getFnc(),order.getKey(),order.getArgument());
        return response;
        
    }

}
