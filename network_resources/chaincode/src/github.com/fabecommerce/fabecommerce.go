/*
Copyright IBM Corp 2016 All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
		 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"bytes"
	"encoding/json"
	"strconv"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// SimpleChaincode example simple Chaincode implementation
type SmartContract struct {
}

type Product struct {
	Pname    string `json:"pname"`
	Type     string `json:"type"`
	State    string `json:"state"`
	Location string `json:"location"`
	Owner    string `json:"owner"`
}

// Init resets all the things
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

// Invoke is our entry point to invoke a chaincode function
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryProduct" {
		return s.queryProduct(APIstub, args)
	}else if function == "queryAllProduct" {
		return s.queryAllProduct(APIstub)
	}else if function == "queryByOwner" {
		return s.queryByOwner(APIstub,args)
	}else if function == "queryByState" {
		return s.queryByState(APIstub,args)
	}else if function == "queryByStateAndOwner" {
		return s.queryByStateAndOwner(APIstub,args)
	}else if function == "initLedger" {
		return s.initLedger(APIstub)
	}else if function == "putProduct" {
		return s.putProduct(APIstub, args)
	}else if function == "changeProductState"{
		return s.changeProductState(APIstub, args)
	}else if function == "changeProductLocation"{
		return s.changeProductLocation(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) queryProduct(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
                return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	productAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(productAsBytes)
}

func (s *SmartContract) queryAllProduct(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := "PRODUCT0"
	endKey := "PRODUCT999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllProduct:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}


func (s *SmartContract) queryByOwner(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	startKey := "PRODUCT0"
	endKey := "PRODUCT999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")
	
	product := Product{}
	
	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
	  json.Unmarshal(queryResponse.Value, &product)
    
		if product.Owner == args[0]{
		        buffer.WriteString("{\"Key\":")
			buffer.WriteString("\"")
			buffer.WriteString(queryResponse.Key)
			buffer.WriteString("\"")

			buffer.WriteString(", \"Record\":")
			// Record is a JSON object, so we write as-is
			buffer.WriteString(string(queryResponse.Value))
			buffer.WriteString("}")
			bArrayMemberAlreadyWritten = true
		}
	}
	buffer.WriteString("]")

	fmt.Printf("- queryProductByOWner:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryByState(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	startKey := "PRODUCT0"
	endKey := "PRODUCT999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	product := Product{}

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
	  json.Unmarshal(queryResponse.Value, &product)

    if product.State == args[0]{
                        buffer.WriteString("{\"Key\":")
			buffer.WriteString("\"")
			buffer.WriteString(queryResponse.Key)
			buffer.WriteString("\"")

			buffer.WriteString(", \"Record\":")
			// Record is a JSON object, so we write as-is
			buffer.WriteString(string(queryResponse.Value))
			buffer.WriteString("}")
			bArrayMemberAlreadyWritten = true
		}
	}
	buffer.WriteString("]")

	fmt.Printf("- queryProductByState:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryByStateAndOwner(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	startKey := "PRODUCT0"
	endKey := "PRODUCT999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	product := Product{}

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
	  json.Unmarshal(queryResponse.Value, &product)
		
		if product.Owner == args[0] && product.State == args[1] {
			buffer.WriteString("{\"Key\":")
			buffer.WriteString("\"")
			buffer.WriteString(queryResponse.Key)
			buffer.WriteString("\"")

			buffer.WriteString(", \"Record\":")
			// Record is a JSON object, so we write as-is
			buffer.WriteString(string(queryResponse.Value))
			buffer.WriteString("}")
			bArrayMemberAlreadyWritten = true
		}
	}
	buffer.WriteString("]")

	fmt.Printf("- queryProductByStateAndOwner:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	product := []Product{
		Product{Pname: "Laptop", Type: "Office", State: "Delivered", Location : "USA", Owner: "John"},
		Product{Pname: "Monitor", Type: "Office", State: "Delivered", Location: "Germany", Owner: "Kim"},
		Product{Pname: "Table", Type: "Home", State: "Delivered", Location: "Italy", Owner: "Carl"},
		Product{Pname: "Chair", Type: "Home", State: "Delivered", Location: "Greece", Owner: "Jess"},
		Product{Pname: "IPhone", Type: "Mobile", State: "Delivered", Location: "Spain", Owner: "Jim"},
		Product{Pname: "Samsung", Type: "Mobile", State: "Delivered", Location: "Bulgary", Owner: "Rick"},
	}

	i := 0
	for i < len(product) {
		fmt.Println("i is ", i)
		productAsBytes, _ := json.Marshal(product[i])
		APIstub.PutState("PRODUCT" + strconv.Itoa(i), productAsBytes)
		fmt.Println("Added", product[i])
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) putProduct(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	i := 0
	startKey := "PRODUCT0"
	endKey := "PRODUCT999"

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 6")
	}

	product := Product{Pname: args[0], Type: args[1], State: args[2], Location: args[3], Owner: args[4]}


	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		fmt.Println("Record", string(queryResponse.Value))
		i= i + 1
	}
	
	productAsBytes, _ := json.Marshal(product)
	APIstub.PutState("PRODUCT" + strconv.Itoa(i), productAsBytes)
	fmt.Println("Added", product)
	return shim.Success(nil)
}

func (s *SmartContract) changeProductState(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	productAsBytes, _ := APIstub.GetState(args[0])
	product := Product{}

	json.Unmarshal(productAsBytes, &product)
	product.State = args[1]

	productAsBytes, _ = json.Marshal(product)
	APIstub.PutState(args[0], productAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) changeProductLocation(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	productAsBytes, _ := APIstub.GetState(args[0])
	product := Product{}

	json.Unmarshal(productAsBytes, &product)
	product.Location = args[1]

	productAsBytes, _ = json.Marshal(product)
	APIstub.PutState(args[0], productAsBytes)

	return shim.Success(nil)
}


func main() {
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error starting SmartContract: %s", err)
	}
}
