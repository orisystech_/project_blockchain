import React, { Component } from 'react';
import { StyleSheet,ActivityIndicator, Image,TextInput, View, ScrollView, Text, TouchableOpacity,FlatList, AsyncStorage ,Dimensions } from 'react-native';
import { StackNavigator } from 'react-navigation';
import {localhost} from '../localhost.js'
//Constants
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
const cardHeight = screenHeight * 0.5;
const cardWidth = screenWidth * 0.95;
import {product} from '../components/entries.js'
import Modal from "react-native-modal";

export default class ProcessOrder extends Component {

    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this)
        this._keyExtractor = this._keyExtractor.bind(this)
        this.data = this.props.navigation.state.params.data
        this.state = {
            modalVisible: false,
            location: ""
        }
    }

    image(category,productName) {
        for (i=0;i<product.length;i++) {
            if (product[i].category==category) {
                let data = product[i].data
                for (j=0;j<data.length;j++) {
                    if (data[j].title == productName) {
                        return (
                            <Image source={data[j].illustration} style={styles.image_product}>
                            </Image>
                        );
                    }
                }
            }
        }
    }


    async fetchChangeProductState(key,state) {

        console.log("fetch to change state")
        response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/changeProductOrdered', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({fnc: 'changeProductState',key: key,argument:state})
        })
        responseText = await response.text();
        console.log(" risultato " + responseText)
        value = await AsyncStorage.getItem('transactions');
        if (value != null) {
            console.log("add " + responseText + " in storage")
            transactionsId = JSON.parse(value)
            transactionsId.push(responseText)
        } else {
            console.log("create array transactionsId from " + responseText + " in storage")
            transactionsId = [responseText]
        }

        await AsyncStorage.setItem('transactions', JSON.stringify(transactionsId))

        
    }

    async fetchChangeProductLocation(key,location) {
        console.log("fetch to change location")
        response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/changeProductOrdered', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({fnc: 'changeProductLocation',key: key, argument: location})
        })
        responseText = await response.text();
        console.log(" risultato " + responseText)
        value = await AsyncStorage.getItem('transactions');
        if (value != null) {
            console.log("add " + responseText + " in storage")
            transactionsId = JSON.parse(value)
            transactionsId.push(responseText)
        } else {
            console.log("create array transactionsId from " + responseText + " in storage")
            transactionsId = [responseText]
        }

        await AsyncStorage.setItem('transactions', JSON.stringify(transactionsId))
    }

    async fetchAndNavigate(key,location,state) {
        this.setState({modalVisible: true})
        await this.fetchChangeProductState(key,state)
        setTimeout(async () => {
        await this.fetchChangeProductLocation(key,location)
                  .then(() => {
                      this.setState({modalVisible: false})
                      this.props.navigation.navigate("Home")
                  })
        },2000)
    }

    renderModal() {
            return (
                <Modal
                    isVisible={this.state.modalVisible}
                    transparent={true}
                    backgroundOpacity={0}
                    animationType='fade'
                >
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <ActivityIndicator size="large" color="yellow" />
                    </View>
                </Modal>

            )
    }
    
    _renderItem({item}) { 
        return(
            <View style={styles.cardView}>
                <View style={{flexDirection: 'row'}} >
                    {this.image(item.Record["type"],item.Record["pname"])}
                    <View style={{flexDirection:'column', marginTop:15}}>
                        <View style={styles.lineStyle}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}>Product Name: </Text>
                                <Text style={{fontSize: 18}}>{item.Record['pname']}</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}>Location: </Text>
                                <Text style={{fontSize: 18}}>{item.Record['location']}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}>Buyer: </Text>
                                <Text style={{fontSize: 18}}>{item.Record['owner']}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}>Order number: </Text>
                                <Text style={{fontSize: 12, fontWeight:'bold', marginTop:5}}>{item.Key}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}>Stato: </Text>
                                <Text style={{fontSize: 18}}>{item.Record['state']}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{alignItems:'center', justifyContent:'center', marginTop: 5}}>
                    <TextInput
                        autoCapitalize='words'
                        placeholderTextColor='black'
                        clearButtonMode='while-editing'
                        style={{marginTop:30,height:40,width:window.width*0.4,borderColor: 'gray', backgroundColor: 'white',alignItems:'center',justifyContent:'center',borderWidth: 1}}
                        onChangeText={(location) => this.setState({location})}
                        value={this.state.location}
                    />
                    <View style={{flexDirection: "row", alignItems:'center', justifyContent:'space-around', marginTop: 20}}>
                        <TouchableOpacity style={{height: 30, width: 180, backgroundColor:'#fff', justifyContent:'center', alignItems:'center', borderRadius: 50}} onPress={ () => this.fetchAndNavigate(item.Key,this.state.location,"shipped")}>
                            <Text style={{fontSize: 18, fontWeight:'bold', color:'#0c1660'}}> SHIP TO </Text> 
                        </TouchableOpacity>
                        <TouchableOpacity style={{height: 30, width: 180, backgroundColor:'#fff', justifyContent:'center', alignItems:'center', borderRadius: 50}} onPress={ () => this.fetchAndNavigate(item.Key," ","complete")}>
                            <Text style={{fontSize: 18, fontWeight:'bold', color:'#0c1660'}}> COMPLETE </Text> 
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

    _keyExtractor(item,index) {
        return item.Key
    }

    render() {
        console.log("data " + JSON.stringify(this.data))
        return (
            <ScrollView style={styles.container}>
                <View style={{alignItems:'center'}}>
                    <FlatList
                        data={this.data}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                    >
                    </FlatList>
                    {this.renderModal()}
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: 'rgba(31,32,32,0.8)'
    },
    image_product: {
        resizeMode: 'center',
        height: 120,
        width: 120,
        marginLeft: 5,
        marginTop:10,
        borderRadius: 4
    },
    cardView: {
        backgroundColor: '#cccccc',
        borderRadius: 8,
        height: cardHeight,
        width: cardWidth,
        marginBottom: 20
    },
    textDettagliOrdine: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 8,
        marginTop: 10
    },
    lineStyle: {
        borderColor: '#efefef',
        borderRadius: 3,
        borderBottomWidth: 2,
        borderRightWidth: 2, 
        marginLeft: 5,
        marginBottom: 10, 
        width: cardWidth * 0.60
    }
});
