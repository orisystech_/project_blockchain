import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet,ActivityIndicator, View, Text, Image } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { RootStuck } from '../App';
import {localhost} from '../localhost.js'
import Modal from "react-native-modal";

export default class CourierView extends Component {
    static navigationOptions = {
        title: 'Courier',
        headerStyle: {
          backgroundColor: 'rgba(44,196,181,0.8)',
        },
        headerTintColor: 'rgba(24,24,26,0.8)',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
    }

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        }
    }

    async fetchQueryByState() {
        let response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/queryByState', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({state: "processed",owner: null})
        })
        let responseProductProcessed = await response.json();
        console.log(" risultato processed product " + JSON.stringify(responseProductProcessed))
        response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/queryByState', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({state: "shipped",owner: null})
        })
        let responseProductShipped = await response.json();
        console.log(" risultato shipped product " + JSON.stringify(responseProductShipped))

        this.setState({modalVisible: false})

        return responseProductProcessed.concat(responseProductShipped)

    }


    fetchAndNavigate() {
        this.setState({modalVisible: true})
        this.fetchQueryByState().then((data) => this.props.navigation.navigate('ProcessedSpedition',{data:data}))
    }

    renderModal() {
        return (
            <Modal
                isVisible={this.state.modalVisible}
                transparent={true}
                backgroundOpacity={0}
                animationType='fade'
            >
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <ActivityIndicator size="large" color="yellow" />
                </View>
            </Modal>

        )
    }

    render () {
        const screen = this.props;

        return (
            <View style={styles.container}>
                
                <TouchableOpacity style={styles.square} onPress={() => this.props.navigation.navigate('Transactions')}>
                    <Text style={styles.text}> Block Explorer </Text>
                    <Image style={styles.img} source={require('../icon/blockExplorer.png')}/> 
                </TouchableOpacity>

                <TouchableOpacity style={styles.square} onPress={() => this.fetchAndNavigate()}>
                    <Text style={styles.text}> Process Spedition </Text>
                    <Image style={styles.img} source={require('../icon/clipboard.png')}/>
                </TouchableOpacity>
                {this.renderModal()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'rgba(31,32,32,0.8)',
        justifyContent: 'center',
        alignItems:'center'
    },
    square: {
        width: 200,
        height: 200,
        borderRadius: 10,
        marginTop: 20,
        backgroundColor: 'rgba(255,255,255,0.8)',
        alignItems:'center',
        overflow: 'hidden'
    },
    img: {
        resizeMode: 'center',
        height: 170,
        width: 170,
        alignSelf: 'center' 
    },
    text:{
        backgroundColor:'rgba(165,166,173,1)',
        borderRadius:10,
        width: 230,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    }

});
