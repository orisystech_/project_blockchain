import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';
import styles from '../styles/SliderEntry.style';
import { ENTRIES1 } from './entries';
import { StackNavigator } from 'react-navigation';
import { RootStuck } from '../App';


export default class SliderEntry extends Component {
    constructor(props) {
        super(props)
        this.navigate = this.navigate.bind(this)
    }
    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };

    get image () {
        const { data: { illustration }, parallax, parallaxProps, even } = this.props;

        return parallax ? (
            <ParallaxImage
                source={illustration}
                containerStyle={[styles.imageContainer]}
                style={styles.image}
                parallaxFactor={0.35}
                {...parallaxProps}
            />
        ) : (
            <Image source={illustration} style={styles.image}>
            </Image>
        );
    }

    navigate(screen) {
        this.props.navigation.navigate('Client')
    }

    render() {
        const { data: { title, subtitle }, even,screen } = this.props;

        const uppercaseTitle = title ? (
            <Text
                style={[styles.title, even ? styles.titleEven : {}]}
                numberOfLines={2}
            >
                { title.toUpperCase() }
            </Text>
        ) : false;

        return (
            <TouchableOpacity
                activeOpacity={1}
                style={styles.slideInnerContainer}
                onPress={() => this.props.navigation.navigate(screen)}
            >
                <View style={styles.shadow} />
                <View style={[styles.imageContainer]}>
                    { this.image }
                    <View style={[styles.radiusMask]} />
                </View>
                <View style={[styles.textContainer]}>
                    { uppercaseTitle }
                    <Text
                        style={[styles.subtitle]}
                        numberOfLines={2}
                    >
                        { subtitle }
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}
