import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors } from './index.style';

const IS_IOS = Platform.OS === 'ios';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.70;
const slideWidth = wp(80);
const itemHorizontalMargin = wp(2);

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 3;

const entryBorderRadius = 8;

export default StyleSheet.create({
    slideInnerContainer: {
        width: itemWidth,
        height: slideHeight,
        paddingHorizontal: itemHorizontalMargin,
        paddingBottom: 18, // needed for shadow
        paddingTop: 25
    },
    slideInnerContainer_product: {
        width: 270,
        height: 270,
        paddingHorizontal: 5,
        paddingBottom: 18, // needed for shadow
        paddingTop: 20
    },
    shadow: {
        position: 'absolute',
        top: 0,
        left: itemHorizontalMargin,
        right: itemHorizontalMargin,
        bottom: 18,
        shadowColor: colors.black,
        shadowOpacity: 0.25,
        shadowOffset: { width: 0, height: 10 },
        shadowRadius: 18,
        borderRadius: entryBorderRadius
    },
    imageContainer: {
        flex: 1,
        marginBottom: IS_IOS ? 0 : -1, // Prevent a random Android rendering issue
        paddingTop: 20,
        backgroundColor: 'rgba(224,227,220,0.8)',
        borderTopLeftRadius: entryBorderRadius,
        borderTopRightRadius: entryBorderRadius
    },
    imageContainer_product: {
        flex: 1,
        marginBottom: IS_IOS ? 0 : -1,
        backgroundColor: 'rgba(224,227,220,0.8)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'center',
        borderRadius: IS_IOS ? entryBorderRadius : 0,
        borderTopLeftRadius: entryBorderRadius,
        borderTopRightRadius: entryBorderRadius
    },
    image_product: {
        resizeMode: 'center',
        borderRadius: IS_IOS ? entryBorderRadius : 0,
        borderTopLeftRadius: entryBorderRadius,
        borderTopRightRadius: entryBorderRadius
    },
    textContainer: {
        justifyContent: 'center',
        paddingTop: 20 - entryBorderRadius,
        paddingHorizontal: 16,
        backgroundColor: 'rgba(224,227,220,0.8)',
        borderBottomLeftRadius: entryBorderRadius,
        borderBottomRightRadius: entryBorderRadius
    },
    textContainer_product: {
        // flexDirection: 'row',
        // justifyContent: 'space-between',
        paddingTop: 5 ,
        paddingBottom: 5,
        paddingHorizontal: 10,
        backgroundColor: 'rgba(224,227,220,0.8)',
        borderBottomLeftRadius: entryBorderRadius,
        borderBottomRightRadius: entryBorderRadius
    },
    title: {
        color: colors.black,
        fontSize: 18,
        fontWeight: 'bold',
        letterSpacing: 0.5
    },
    title_product: {
        color: colors.black,
        fontSize: 18,
        fontWeight: 'bold',
        letterSpacing: 0.5
    },
    subtitle: {
        marginTop: 6,
        color: colors.gray,
        fontSize: 12,
        fontStyle: 'italic'
    },
    price: {
        position: 'absolute',
        top: 175,
        left: 210,
        fontSize: 17,
        borderRadius: 2,
        fontWeight: 'bold',
        backgroundColor: 'yellow',
    }
});
