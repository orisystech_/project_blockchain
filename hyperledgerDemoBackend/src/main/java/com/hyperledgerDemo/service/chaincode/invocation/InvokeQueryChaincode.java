package com.hyperledgerDemo.service.chaincode.invocation;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.hyperledgerDemo.config.client.CAClient;
import com.hyperledgerDemo.config.client.ChannelClient;
import com.hyperledgerDemo.config.client.FabricClient;
import com.hyperledgerDemo.config.Config;
import com.hyperledgerDemo.user.UserContext;
import com.hyperledgerDemo.util.Util;
import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.EventHub;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.TransactionProposalRequest;

public class InvokeQueryChaincode {

    private static final byte[] EXPECTED_EVENT_DATA = "!".getBytes(UTF_8);
    private static final String EXPECTED_EVENT_NAME = "event";

    public static String queryByOwner(String owner) {
        String stringResponse = null; 
        try {
            Util.cleanUp();
            String caUrl = Config.CA_ORG1_URL;
            CAClient caClient = new CAClient(caUrl, null);
            // Enroll Admin to Org1MSP
            UserContext adminUserContext = new UserContext();
            adminUserContext.setName(Config.ADMIN);
            adminUserContext.setAffiliation(Config.ORG1);
            adminUserContext.setMspId(Config.ORG1_MSP);
            caClient.setAdminUserContext(adminUserContext);
            adminUserContext = caClient.enrollAdminUser(Config.ADMIN, Config.ADMIN_PASSWORD);
			
            FabricClient fabClient = new FabricClient(adminUserContext);
			
            ChannelClient channelClient = fabClient.createChannelClient(Config.CHANNEL_NAME);
            Channel channel = channelClient.getChannel();
            Peer peer = fabClient.getInstance().newPeer(Config.ORG1_PEER_0, Config.ORG1_PEER_0_URL);
            EventHub eventHub = fabClient.getInstance().newEventHub("eventhub01", "grpc://localhost:7053");
            Orderer orderer = fabClient.getInstance().newOrderer(Config.ORDERER_NAME, Config.ORDERER_URL);
            channel.addPeer(peer);
            channel.addEventHub(eventHub);
            channel.addOrderer(orderer);
            channel.initialize();

            String[] args = {owner};

            Collection<ProposalResponse>  responsesQuery = channelClient.queryByChainCode("fabecommerce", "queryByOwner", args);
            for (ProposalResponse pres : responsesQuery) {
                stringResponse = new String(pres.getChaincodeActionResponsePayload());
                System.out.println(stringResponse);
            }

            Thread.sleep(10000);

            return stringResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringResponse;
    }

    public static String queryByState(String state) {
        String stringResponse = null; 
        try {
            Util.cleanUp();
            String caUrl = Config.CA_ORG1_URL;
            CAClient caClient = new CAClient(caUrl, null);
            // Enroll Admin to Org1MSP
            UserContext adminUserContext = new UserContext();
            adminUserContext.setName(Config.ADMIN);
            adminUserContext.setAffiliation(Config.ORG1);
            adminUserContext.setMspId(Config.ORG1_MSP);
            caClient.setAdminUserContext(adminUserContext);
            adminUserContext = caClient.enrollAdminUser(Config.ADMIN, Config.ADMIN_PASSWORD);
			
            FabricClient fabClient = new FabricClient(adminUserContext);
			
            ChannelClient channelClient = fabClient.createChannelClient(Config.CHANNEL_NAME);
            Channel channel = channelClient.getChannel();
            Peer peer = fabClient.getInstance().newPeer(Config.ORG1_PEER_0, Config.ORG1_PEER_0_URL);
            EventHub eventHub = fabClient.getInstance().newEventHub("eventhub01", "grpc://localhost:7053");
            Orderer orderer = fabClient.getInstance().newOrderer(Config.ORDERER_NAME, Config.ORDERER_URL);
            channel.addPeer(peer);
            channel.addEventHub(eventHub);
            channel.addOrderer(orderer);
            channel.initialize();

            String[] args = {state};

            Collection<ProposalResponse>  responsesQuery = channelClient.queryByChainCode("fabecommerce", "queryByState", args);
            for (ProposalResponse pres : responsesQuery) {
                stringResponse = new String(pres.getChaincodeActionResponsePayload());
                System.out.println(stringResponse);
            }

            Thread.sleep(10000);

            return stringResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringResponse;
    }

    public static String queryByStateAndOwner(String owner,String state) {
        String stringResponse = null; 
        try {
            Util.cleanUp();
            String caUrl = Config.CA_ORG1_URL;
            CAClient caClient = new CAClient(caUrl, null);
            // Enroll Admin to Org1MSP
            UserContext adminUserContext = new UserContext();
            adminUserContext.setName(Config.ADMIN);
            adminUserContext.setAffiliation(Config.ORG1);
            adminUserContext.setMspId(Config.ORG1_MSP);
            caClient.setAdminUserContext(adminUserContext);
            adminUserContext = caClient.enrollAdminUser(Config.ADMIN, Config.ADMIN_PASSWORD);
			
            FabricClient fabClient = new FabricClient(adminUserContext);
			
            ChannelClient channelClient = fabClient.createChannelClient(Config.CHANNEL_NAME);
            Channel channel = channelClient.getChannel();
            Peer peer = fabClient.getInstance().newPeer(Config.ORG1_PEER_0, Config.ORG1_PEER_0_URL);
            EventHub eventHub = fabClient.getInstance().newEventHub("eventhub01", "grpc://localhost:7053");
            Orderer orderer = fabClient.getInstance().newOrderer(Config.ORDERER_NAME, Config.ORDERER_URL);
            channel.addPeer(peer);
            channel.addEventHub(eventHub);
            channel.addOrderer(orderer);
            channel.initialize();
            String[] args = {owner,state};	
            Collection<ProposalResponse>  responsesQuery = channelClient.queryByChainCode("fabecommerce", "queryByStateAndOwner", args );
            for (ProposalResponse pres : responsesQuery) {
                stringResponse = new String(pres.getChaincodeActionResponsePayload());
                System.out.println(stringResponse);
            }

            Thread.sleep(10000);

            return stringResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringResponse;
    }

    
}
