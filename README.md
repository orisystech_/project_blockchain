- [HYPERLEDGER](#org6bfad62)
  - [HYPERLEDGER FABRIC MODEL](#org4a2552e)
    - [assets](#orgfbba58c)
    - [chaincde](#orga3db576)
    - [features](#org55bad2b)
    - [privacy grazie ai canali](#orgdffb4b2)
    - [security e membership service](#org0a70067)
    - [consenso](#orgdce0de0)
  - [identity](#orga61930a)
    - [PKI](#org0624e06)
    - [certificati digitali](#orgba404db)
    - [Certificate Authorities](#orgb928a4e)
    - [Root CAs, Intermediate CAs and Chains of Trust](#org7c6ea2a)
    - [Fabric CA](#org80db95d)
    - [Certificate Revocation Lists](#orgc20ca2f)
  - [membership](#orgbfb80fe)
    - [Mapping MSPs to Organizations](#orgf1ed55a)
    - [Organizational Units and MSPs](#org56dbad6)
    - [Local and Channel MSPs](#orgd1dfa7b)
    - [MSP Levels](#org0921388)
    - [MSP Structure](#org6005e73)
  - [Peers](#orge6e9e20)
    - [Ledgers and Chaincode](#orgd9a44b3)
    - [Multiple Ledgers](#org2227699)
    - [Multiple Chaincodes](#org48d4570)
    - [Applications and Peers](#org806aaf4)
    - [Peers and Channels](#org09c414b)
    - [Peers and Organizations](#org5050f65)
    - [Peers and Identity](#org1574e75)
    - [Peers and Orderers](#orgb0feec8)
    - [Orderers and Consensus](#org1e87002)
  - [Ledger](#org807dc4a)
    - [Chain](#orgd279ebe)
    - [State Database](#org2975c24)
    - [Transaction Flow](#orgfcc23e2)


<a id="org6bfad62"></a>

# HYPERLEDGER


<a id="org4a2552e"></a>

## HYPERLEDGER FABRIC MODEL


<a id="orgfbba58c"></a>

### assets

gli assets possono essere cose tangibili o intangibili come per esempio contratti hyperledger fabric dà la possibilità di cambiare il valore di questi assets attraverso il chaincode. in hyperledger fabric gli assets sono una coppia di chiave-valore. gli asset sono memoriazzati come record dentro il ledger del canale.


<a id="orga3db576"></a>

### chaincde

il chaincode è un "programma" che incorpora la business logica, può creare e modificare gli assets.


<a id="org55bad2b"></a>

### features

il ledger (libro mastro) è una sequenza di record a prova di manomissione che contiene tutti i risultati delle transazioni. il risultato delle transazioni è un set di asset chiave valore che viene committato sul ledger. Le possibili azioni sono update, delete,create. ovviamente il ledger contiene la blockchain e l'ultimo stato degli assets. quindi si può dire che contiene un database con gli ultimi valori degli assets e la blockchain con tutta la storia dei blocchi immutabili. c'è un ledger per ogni canale e una copia del ledger è su tutti i peer. possiamo avere:

-   query e update usando il valore della chiave;
-   read-only query usando un linguaggio di query per esempio CouchDB;
-   read-only storia delle query;
-   una transazione consiste di una chiave/valore che è stata letta dal chaincode e da una chiave/valore che è stata scritta;
-   una transazione è composta da ogni signature degli endorsing peer e viene mandata all'endorsing service;
-   le transazioni sono organizzate in blocchi e poi sono mandate ai diversi peer sul canale;
-   i peer convalidano la transazione segundo le politiche di approvazione;
-   prima di accodare un blocco, viene eseguito un controllo di versione per garantire che gli stati per gli assets che sono stati letti non siano cambiati dal momento dell'esecuzione del chaincode;
-   ci sarà l'immutabilità una volta che la transazione viene accettata e messa nel blocco;
-   il canale del ledger che contine la configurazione dei blocchi definisce le politiche, la lista degli accessi di controllo e altre informazioni pertinenti;
-   I canali contengono istanze di membership service provider (MSP) che consentono di ricavare materiali crittografici da diverse autorità di certificazione;


<a id="orgdffb4b2"></a>

### privacy grazie ai canali

Hyperledger Fabric utilizza un ledger immutabile per per ogni canale, nonché il chaincode che può manipolare e modificare lo stato corrente delle risorse (ad esempio, aggiornare coppie di valori chiave). Lo scope del ledger è quello del canale - può essere condiviso su tutta la rete (supponendo che ogni partecipante stia operando su un canale comune) - oppure può essere privatizzato per includere solo una serie specifica di partecipanti. In quest'ultimo scenario, questi partecipanti creerebbero un canale separato e quindi isolerebbero / separerebbero le loro transazioni e il loro ledger. Per risolvere scenari che vogliono colmare il divario tra trasparenza totale e privacy, il chaincode può essere installato solo sui peer che devono accedere agli stati delle risorse per eseguire operazioni di lettura e scrittura.


<a id="org0a70067"></a>

### security e membership service

Hyperledger Fabric sostiene una rete transazionale in cui tutti i partecipanti hanno identità conosciute. Public Key Infrastructure viene utilizzato per generare certificati crittografici legati a organizzazioni, componenti di rete e utenti finali o applicazioni client. Di conseguenza, il controllo dell'accesso ai dati può essere manipolato e regolato sulla rete più ampia e sui livelli di canale.


<a id="orgdce0de0"></a>

### consenso

Il consenso viene raggiunto quando l'ordine e i risultati delle transazioni di un blocco hanno soddisfatto i criteri espliciti della policy. Questi controlli e saldi hanno luogo durante il ciclo di vita di una transazione e includono l'utilizzo di politiche di approvazione per stabilire quali membri specifici devono sostenere una determinata classe di transazione, nonché il chaincode del sistema per garantire che tali politiche siano applicate e confermate. Prima di impegnarsi, i peer impiegheranno questo chaincode di sistema per assicurarsi che siano presenti sufficienti approvazioni e che siano state derivate dalle entità appropriate. Inoltre, verrà effettuato un controllo di versione durante il quale lo stato corrente del ledger è concordato o consentito, prima che eventuali blocchi contenenti transazioni vengano aggiunti sul ldeger. Questo controllo finale fornisce protezione contro le operazioni di double spending e altre minacce che potrebbero compromettere l'integrità dei dati e consente l'esecuzione di funzioni contro variabili non statiche.


<a id="orga61930a"></a>

## identity

I diversi attori di una rete blockchain includono peer, ordereer, applicazioni client, amministratori e altro. Ognuno di questi attori ha un'identità incapsulata in un certificato digitale X.509. Queste identità sono importanti perché determinano le autorizzazioni esatte sulle risorse che gli attori hanno in una rete blockchain. L'implementazione MSP predefinita in Fabric utilizza i certificati X.509 come identità, adottando un modello gerarchico tradizionale PKI (Public Key Infrastructure).


<a id="org0624e06"></a>

### PKI

Public Key Infrastructure (PKI) è una raccolta di tecnologie Internet che fornisce comunicazioni sicure in una rete. È PKI che mette la S in HTTPS Un PKI è composto da autorità di certificazione che emettono certificati digitali per le parti (ad es. Utenti di un servizio, fornitore di servizi), che li utilizzano per autenticarsi nei messaggi scambiati con il proprio ambiente. Certification Revocation List (CRL) di una Certiication Authority (CA) costituisce un riferimento per i certificati che non sono più validi. ci sono quattro elementi chiave nella PKI:

-   Digital Certificates
-   Public and Private Keys
-   Certificate Authorities
-   Certificate Revocation Lists


<a id="orgba404db"></a>

### certificati digitali

Un certificato digitale è un documento che contiene una serie di attributi relativi a un party. Il tipo più comune di certificato è quello conforme allo standard X.509, che consente la codifica dei dettagli identificativi di una parte nella sua struttura.

La crittografia consente di presentare il proprio certificato ad altri per dimostrare la propria identità fintanto che l'altra parte si fida dell'autorità di certificazione, nota come Certificate Authority (CA). Finché la CA mantiene certe informazioni crittografiche in modo sicuro (cioè la propria chiave privata di firma), chiunque legga il certificato può essere certo che le informazioni non siano state manomesse. Pensa al certificato X.509 come a una carta d'identità digitale che è impossibile cambiare.


<a id="orgb928a4e"></a>

### Certificate Authorities

Un attore o un nodo è in grado di partecipare alla rete blockchain, attraverso i mezzi di un'identità digitale rilasciata per quest'ultimo da un'autorità fidata dal sistema. Nel caso più comune, le identità digitali (o semplicemente le identità) hanno la forma di certificati digitali convalidati crittograficamente conformi allo standard X.509 e rilasciati da una Certificate Autority (CA). Una Certificate Authority eroga i certificati a diversi attori. Questi certificati sono firmati digitalmente dalla CA (cioè, utilizzando la chiave privata della CA), e rilevano insieme l'attore reale con la chiave pubblica dell'attore, e facoltativamente con un elenco completo di proprietà. Chiaramente, se uno si fida della CA (e conosce la sua chiave pubblica), può (convalidando la firma della CA sul certificato dell'attore) fidarsi che l'attore specifico è associato alla chiave pubblica inclusa nel certificato e possiede gli attributi inclusi.

I certificati cruciali possono essere ampiamente diffusi, in quanto non includono né le chiavi private degli attori né quelle effettive della CA. Come tali possono essere usati come ancoraggio di trust per autenticare messaggi provenienti da diversi attori.

Nell'impostazione Blockchain, ogni attore che desidera interagire con la rete ha bisogno di un'identità. In questa impostazione, si potrebbe dire che una o più CA possono essere utilizzate per definire i membri di un'organizzazione da una prospettiva digitale. È la CA che fornisce la base per gli attori di un'organizzazione per avere un'identità digitale verificabile.


<a id="org7c6ea2a"></a>

### Root CAs, Intermediate CAs and Chains of Trust

Le CA sono disponibili in due versioni: CA principali e CA intermedie. Poiché le CA radice (Symantec, Geotrust, ecc.) Devono distribuire in modo sicuro centinaia di milioni di certificati agli utenti di Internet, è opportuno distribuire questo processo tra le cosiddette CA intermedie. Queste CA intermedie hanno i loro certificati emessi dalla CA radice o un'altra autorità intermedia, consentendo l'istituzione di una "catena di fiducia" per qualsiasi certificato emesso da qualsiasi CA nella catena.


<a id="org80db95d"></a>

### Fabric CA

le CA sono così importanti che Fabric fornisce un componente CA integrato che consente di creare CA nelle reti di blockchain che si formano. Questo componente, noto come fabric-ca, è un provider di CA pincipale privato in grado di gestire le identità digitali dei partecipanti Fabric che hanno la forma di certificati X.509. Poiché Fabric-CA è una CA personalizzata che mira alle esigenze della CA principale di Fabric, non è intrinsecamente in grado di fornire certificati SSL per l'utilizzo generale/automatico nei browser. Tuttavia, poiché alcuni CA devono essere utilizzati per gestire l'identità (anche in un ambiente di prova), fabric-ca può essere utilizzato per fornire e gestire i certificati. È anche possibile - e pienamente appropriato - utilizzare una root pubblica / commerciale o una CA intermedia per fornire l'identificazione.


<a id="orgc20ca2f"></a>

### Certificate Revocation Lists

Un elenco di certificati revocati (CRL) è di facile comprensione: è solo un elenco di riferimenti ai certificati che un'autorità di certificazione sa essere revocata per un motivo o per un altro. un CRL sarebbe come un elenco di carte di credito rubate. Quando una terza parte desidera verificare l'identità di un'altra parte, controlla innanzitutto il CRL della CA emittente per assicurarsi che il certificato non sia stato revocato. Un verificatore non deve controllare il CRL, ma se non lo fa corrono il rischio di accettare un'identità compromessa.


<a id="orgbfb80fe"></a>

## membership

abbiamo visto come una PKI può fornire identità verificabili attraverso una catena di fiducia. Ora vediamo come queste identità possono essere utilizzate per rappresentare i membri fidati di una rete blockchain. È qui che entra in gioco un MSP (Member Service Provider) che identifica le CA radice e le CA intermedie per definire i membri di un dominio di fiducia, ad esempio un'organizzazione, elencando le identità dei membri o identificando quali CA sono autorizzate a rilasciare identità valide per i propri membri o, come di solito, attraverso una combinazione di entrambi.

La potenza di un MSP va oltre la semplice elencazione di chi è un partecipante alla rete o membro di un canale. Un MSP può identificare ruoli specifici che un attore può svolgere all'interno dell'organizzazione (dominio di fiducia) che l'MSP rappresenta (ad es. Amministratori MSP, membri di una suddivisione aziendale) e pone le basi per la definizione dei privilegi di accesso nel contesto di una rete e un canale (ad esempio, amministratori di canale, lettori, scrittori). La configurazione di un MSP è pubblicata su tutti i canali, dove partecipano i membri dell'organizzazione corrispondente (nella forma di un canale MSP). Anche peer, orderer e client mantengono un'istanza MSP locale (nota anche come lLocal MSP) per autenticare i messaggi dei membri della loro organizzazione al di fuori del contesto di un canale. Inoltre, un MSP può consentire l'identificazione di un elenco di identità che sono state revocate.


<a id="orgf1ed55a"></a>

### Mapping MSPs to Organizations

La cosa più importante riguardo alle organizzazioni (o organizzazioni) è che gestiscono i loro membri con un unico MSP. La relazione esclusiva tra un'organizzazione e la sua MSP rende ragionevole nominare l'MSP dopo l'organizzazione, una convenzione che troverai adottata nella maggior parte delle configurazioni delle policy. Ad esempio, l'organizzazione ORG1 avrebbe un MSP chiamato ORG1-MSP. In alcuni casi un'organizzazione può richiedere più gruppi di appartenenza, ad esempio, dove i canali vengono utilizzati per svolgere funzioni aziendali molto diverse tra le organizzazioni. In questi casi ha senso avere più MSP e nominarli di conseguenza, ad es. ORG2-MSP-NATIONAL e ORG2-MSP-GOVERNMENT, che riflettono le diverse radici di appartenenza di fiducia all'interno di ORG2 nel canale di vendita NAZIONALE rispetto al canale normativo del GOVERNMENT.


<a id="org56dbad6"></a>

### Organizational Units and MSPs

Un'organizzazione è spesso suddivisa in più unità organizzative (OU), ognuna delle quali ha una serie di responsabilità. Ad esempio, l'organizzazione ORG1 potrebbe avere ORG1-MANUFACTURING e ORG1-DISTRIBUTION OUs per riflettere queste separate linee di business. Quando una CA rilascia certificati X.509, il campo OU nel certificato specifica la linea di business a cui appartiene l'identità. Vedremo in seguito come le OU possono essere utili per controllare le parti di un'organizzazione considerate come membri di una rete blockchain. Ad esempio, solo le identità dall'unità ORG1-MANUFACTURING potrebbero essere in grado di accedere a un canale, mentre ORG1-DISTRIBUTION non può.

Infine, sebbene si tratti di un lieve uso improprio delle unità organizzative, a volte possono essere utilizzati da diverse organizzazioni in un consorzio per distinguersi a vicenda. In tali casi, le diverse organizzazioni utilizzano le stesse CA radice e CA intermedie per la loro catena di fiducia, ma assegnano il campo OU in modo appropriato per identificare i membri di ciascuna organizzazione. Vedremo anche come configurare MSP per ottenere ciò in seguito


<a id="orgd1dfa7b"></a>

### Local and Channel MSPs

Gli MSP appaiono in due punti in una rete Blockchain: nella configurazione del canale (MSP del canale) e localmente nella presupposizione di un attore (MSP locale). MSP locali definiti per i nodi, (peer o orderer) e gli utenti (amministratori che utilizzano la CLI o le applicazioni client che utilizzano l'SDK). Ogni nodo e utente deve avere definito un MSP locale, in quanto definisce chi ha diritti amministrativi o partecipazioni a quel livello e al di fuori del contesto di un canale (ad esempio, gli amministratori dell'organizzazione di un peer).

Al contrario, gli MSP di canale definiscono i diritti amministrativi e le partecipazioni a livello di canale. Per ogni organizzazione che partecipa a un canale deve essere definito un MSP. I peer e gli orderer su un canale condivideranno tutti la stessa vista sugli MSP del canale e d'ora in poi saranno in grado di autenticare correttamente i partecipanti al canale. Ciò significa che se un'organizzazione desidera aderire al canale, un MSP che incorpori la catena di fiducia per i membri dell'organizzazione dovrà essere incluso nella configurazione del canale. In caso contrario, le transazioni provenienti dalle identità di questa organizzazione verranno rifiutate.

Gli MSP locali sono definiti solo sul file system del nodo o dell'utente a cui si applicano. Pertanto, fisicamente e logicamente esiste solo un MSP locale per nodo o utente. Tuttavia, poiché gli MSP del canale sono disponibili per tutti i nodi nel canale, vengono definiti logicamente una volta nel canale nella sua configurazione. Tuttavia, un MSP di canale viene istanziato sul file system di ogni nodo del canale e mantenuto sincronizzato tramite consenso. Quindi, mentre c'è una copia di ogni canale MSP sul file system locale di ogni nodo, logicamente un canale MSP risiede sul canale e sulla rete.

1.  esempio

    Un amministratore B si connette al peer con un'identità emessa da RCA1 e memorizzata nel proprio MSP locale. Quando B tenta di installare uno smart contract sul peer, il peer controlla il suo MSP locale, ORG1-MSP, per verificare che l'identità di B sia effettivamente un membro di ORG1. Una verifica riuscita consentirà il completamento del comando di installazione. Successivamente, B desidera creare un'istanza dello smart contract sul canale. Poiché si tratta di un'operazione di canale, tutte le organizzazioni nel canale devono accettarlo. Pertanto, il peer deve controllare gli MSP del canale prima che possa confermare correttamente questo comando. (Anche altre cose devono accadere, ma per ora concentrati su quanto sopra.)


<a id="org0921388"></a>

### MSP Levels

La suddivisione tra channel e local MSP riflette le esigenze delle organizzazioni di amministrare le proprie risorse locali, come i nodi peer o orderer, e le loro risorse di canale, come registri, smart contract, che operano a livello di canale o di rete. È utile pensare a questi MSP come a diversi livelli, con MSP a un livello più alto in relazione ai problemi di amministrazione della rete, mentre gli MSP a livello inferiore gestiscono l'identità per l'amministrazione di risorse private. Gli MSP sono obbligatori a tutti i livelli di amministrazione: devono essere definiti per rete, canale, peer, ordinatore e utenti.

-   Rete MSP: la configurazione di una rete definisce chi sono i membri della rete - definendo gli MSP delle organizzazioni partecipanti - e quali di questi membri sono autorizzati a svolgere attività amministrative (ad esempio, creando un canale).
-   MSP channel: è importante che un canale mantenga gli MSP dei suoi membri separatamente. Un canale fornisce comunicazioni private tra un particolare gruppo di organizzazioni che a loro volta hanno il controllo amministrativo su di esso. Le policy di canale interpretate nel contesto degli MSP di quel canale definiscono chi ha la capacità di partecipare a determinate azioni sul canale, ad esempio aggiungendo organizzazioni o istanziando chhaincode. Si noti che non esiste alcuna relazione necessaria tra l'autorizzazione ad amministrare un canale e la possibilità di amministrare il canale di configurazione di rete (o qualsiasi altro canale). I diritti amministrativi esistono nell'ambito di ciò che viene amministrato (a meno che le regole non siano state scritte diversamente - vedi la discussione dell'attributo ROLE di seguito).
-   Peer MSP: questo MSP locale è definito sul file system di ciascun peer e esiste una singola istanza MSP per ogni peer. Concettualmente, svolge esattamente la stessa funzione degli MSP di canale con la limitazione che si applica solo al peer dove è definito. Un esempio di un'azione la cui autorizzazione viene valutata utilizzando l'MSP locale del peer è l'installazione di un chaincode sulla premessa del peer.
-   Orderer MSP: come un MSP peer, anche un MSP locale orderer viene definito sul file system del nodo e si applica solo a quel nodo. Come i nodi peer, anche gli orderer sono di proprietà di una singola organizzazione e quindi hanno un singolo MSP per elencare gli attori o i nodi di cui si fida.


<a id="org6005e73"></a>

### MSP Structure

Ci sono nove elementi per un MSP. È più semplice pensare a questi elementi in una struttura di directory, in cui il nome MSP è il nome della cartella root con ogni sottocartella che rappresenta diversi elementi di una configurazione MSP.

-   Root CA: questa cartella contiene un elenco di certificati X.509 autofirmati delle CA principali considerate attendibili dall'organizzazione rappresentate da questo MSP. In questa cartella MSP deve essere presente almeno un certificato Root CA X.509. Questa è la cartella più importante perché identifica le CA da cui devono essere derivati tutti gli altri certificati per essere considerati membri dell'organizzazione corrispondente.

-   Intermediate CA: questa cartella contiene un elenco di certificati X.509 delle CA intermedie considerate attendibili da questa organizzazione. Ogni certificato deve essere firmato da una delle CA principali nel MSP o da una CA intermedia, la cui catena CA di emissione riconduce infine a una CA radice affidabile. Intuitivamente per vedere l'utilizzo di una CA intermedia rispetto alla struttura dell'organizzazione corrispondente, una CA intermedia può rappresentare una suddivisione diversa dell'organizzazione o dell'organizzazione stessa (ad esempio, se una CA commerciale viene sfruttata per la gestione dell'identità dell'organizzazione). In quest'ultimo caso, altre CA intermedie, inferiori nella gerarchia CA, possono essere utilizzate per rappresentare suddivisioni organizzative. questa cartella può essere vuota. Come la cartella Root CA, questa cartella definisce le CA da cui devono essere rilasciati i certificati per essere considerati membri dell'organizzazione.

-   Unità organizzative (OU): sono elencate nel $FABRIC<sub>CFG</sub><sub>PATH</sub>/msp/config.yaml file e contengono un elenco di unità organizzative, i cui membri sono considerati parte dell'organizzazione rappresentata da questo MSP. Ciò è particolarmente utile quando si desidera limitare i membri di un'organizzazione a quelli in possesso di un'identità (firmata da una delle CA designate da MSP) con una specifica unità organizzativa in essa contenuta. La specifica delle unità organizzative è facoltativa. Se non vengono elencate unità organizzative, tutte le identità che fanno parte di un MSP, identificate dalle cartelle Root CA e Intermediate CA, saranno considerate membri dell'organizzazione.

-   Administrator: questa cartella contiene un elenco di identità che definiscono gli attori che hanno il ruolo di amministratori per questa organizzazione. Per il tipo MSP standard, ci dovrebbero essere uno o più certificati X.509 in questo elenco. Vale la pena notare che solo perché un attore ha il ruolo di amministratore non significa che possano amministrare risorse particolari! La potenza effettiva di una determinata identità rispetto all'amministrazione del sistema è determinata dalle politiche che gestiscono le risorse di sistema. Ad esempio, una politica di canale potrebbe specificare che gli ORG1-MANUFACTURING amministratori hanno i diritti per aggiungere nuove organizzazioni al canale, mentre gli ORG1-DISTRIBUTION amministratori non hanno tali diritti. Anche se un certificato X.509 ha un ROLE attributo (specificando, ad esempio, che un attore è un admin), ciò si riferisce al ruolo di un attore all'interno della sua organizzazione piuttosto che alla rete blockchain. Questo è simile allo scopo OU dell'attributo che, se è stato definito, si riferisce al posto di un attore nell'organizzazione. L'attributo ROLE può essere utilizzato per conferire diritti amministrativi a livello di canale se la politica per quel canale è stata scritta per consentire a qualsiasi amministratore da un'organizzazione (o ad alcune organizzazioni) il permesso di eseguire determinate funzioni di canale (come il chaincode di istanziazione). In questo modo, un ruolo dell'organizzazione può conferire un ruolo di rete. Questo è concettualmente simile al modo in cui avere una patente di guida rilasciata dallo stato della Florida negli USA autorizza qualcuno a guidare in ogni stato degli Stati Uniti.

-   Revoked Certificates: se l'identità di un attore è stata revocata, l'identificazione delle informazioni sull'identità, non sull'identità stessa, viene conservata in questa cartella. Per le identità basate su X.509, questi identificatori sono coppie di stringhe note come SKI (Subject Key Identifier) e AKI (Authority Access Identifier) e vengono verificate ogni volta che viene utilizzato il certificato X.509 per assicurarsi che il certificato non sia stato revocato. Questo elenco è concettualmente identico all'elenco di revoche di certificati (CRL) di una CA, ma si riferisce anche alla revoca dell'appartenenza all'organizzazione. Di conseguenza, l'amministratore di un MSP, locale o channel, può revocare rapidamente un attore o un nodo da un'organizzazione pubblicizzando il CRL aggiornato della CA del certificato revocato rilasciato da. Questa "lista di liste" è facoltativa. Diventerà popolato solo quando i certificati verranno revocati.

-   Node Identity: questa cartella contiene l'identità del nodo, ovvero materiale crittografico che, in combinazione con il contenuto di KeyStore, consentirebbe al nodo di autenticarsi nei messaggi inviati agli altri partecipanti dei suoi canali e della rete. Per le identità basate su X.509, questa cartella contiene un certificato X.509 . Questo è il certificato che un peer inserisce in una risposta alla proposta di transazione, ad esempio per indicare che il peer lo ha approvato, che può essere successivamente verificato rispetto alla politica di approvazione della transazione risultante al momento della convalida. Questa cartella è obbligatoria per gli MSP locali e deve esistere esattamente un certificato X.509 per il nodo. Non è utilizzato per gli MSP del canale.

-   KeyStore for Private Key: questa cartella è definita per l'MSP locale di un nodo peer o orderer (o in un MSP locale del client) e contiene la chiave di firma del nodo. Questa chiave corrisponde crittograficamente all'identità del nodo inclusa nella cartella Node Identity e viene utilizzata per firmare i dati, ad esempio per firmare una risposta alla proposta di transazione, come parte della fase di approvazione. Questa cartella è obbligatoria per gli MSP locali e deve contenere esattamente una chiave privata. Ovviamente, l'accesso a questa cartella deve essere limitato solo alle identità, gli utenti che hanno responsabilità amministrativa sul peer. La configurazione di un MSP di canale non include questa parte, poiché gli MSP di canale mirano a offrire esclusivamente funzionalità di convalida dell'identità e non la firma delle abilità.

-   Root CA TLS: questa cartella contiene un elenco di certificati X.509 autofirmati delle CA principali considerate affidabili da questa organizzazione per le comunicazioni TLS. Un esempio di una comunicazione TLS potrebbe essere quando un peer deve connettersi a un ordinatore in modo che possa ricevere gli aggiornamenti di contabilità generale. Le informazioni sul TLS MSP si riferiscono ai nodi all'interno della rete, ovvero i peer e gli ordinatori, piuttosto che quelli che consumano la rete: applicazioni e amministratori. In questa cartella deve essere presente almeno un certificato CA X.509 radice TLS.

-   Intermediate CA TLS: questa cartella contiene un elenco di certificati CA intermedi CA attendibili dall'organizzazione rappresentata da questo MSP per le comunicazioni TLS. Questa cartella è particolarmente utile quando le CA commerciali vengono utilizzate per i certificati TLS di un'organizzazione. Analogamente alle CA intermedie di appartenenza, la specifica delle CA TLS intermedie è facoltativa.


<a id="orge6e9e20"></a>

## Peers

Una rete blockchain è costituita principalmente da un insieme di nodi peer. I peer sono un elemento fondamentale della rete perché ospitano registri e smart contract. Ricorda che un ledger registra immutabilmente tutte le transazioni generate da uno smart contract.Gli smart contract e i ledger vengono utilizzati per incapsulare i processi condivisi e le informazioni condivise in una rete, rispettivamente. Questi aspetti di un peer li rendono un buon punto di partenza per comprendere la rete Hyperledger Fabric.


<a id="orgd9a44b3"></a>

### Ledgers and Chaincode

Diamo un'occhiata a un peer in modo un po' più dettagliato. Possiamo vedere che è il peer che ospita sia il ledger che il chaincode. Più precisamente, il peer ospita effettivamente le istanze del ledger e le istanze del chaincode. Si noti che ciò fornisce una ridondanza deliberata in una rete Fabric, evitando singoli punti di errore. Impareremo di più sulla natura distribuita e decentralizzata di una rete di blockchain più avanti in questo argomento.


<a id="org2227699"></a>

### Multiple Ledgers

Un peer è in grado di ospitare più di un ledger, il che è utile perché consente una progettazione flessibile del sistema. La configurazione peer più semplice è quella di avere un singolo ledger, ma è assolutamente appropriato per un peer ospitare due o più ledger quando richiesto. Sebbene sia perfettamente possibile per un peer ospitare un'istanza del ledger senza ospitare alcun chaincode che lo acceda, è molto raro che i peer siano configurati in questo modo. La stragrande maggioranza dei peer avrà almeno un chaincode installato su di esso che può interrogare o aggiornare le istanze del ledger del peer. Vale la pena menzionare se gli utenti hanno installato o meno chaincode per l'uso da parte di applicazioni esterne, i peer hanno anche dei chaincode di sistema speciali che sono sempre presenti. Questi non sono discussi in dettaglio in questo argomento.


<a id="org48d4570"></a>

### Multiple Chaincodes

Non esiste una relazione fissa tra il numero di ledger di un peer e il numero di chaincode che possono accedere a quel ledger. Un peer potrebbe avere molti chiancode e molti ledger disponibili.


<a id="org806aaf4"></a>

### Applications and Peers

Ora mostreremo come le applicazioni interagiscono con i peer per accedere il ldeger. Le interazioni di query Ledger implicano un semplice dialogo in tre passaggi tra un'applicazione e un peer; le interazioni di aggiornamento del ledger sono un po' più complicate e richiedono due passaggi aggiuntivi. Abbiamo semplificato un po' questi passaggi per aiutarti a iniziare con Hyperledger Fabric, ma non preoccuparti: la cosa più importante da capire è la differenza nelle interazioni tra peer di applicazioni per la query di contabilità generale rispetto agli stili di transazione di aggiornamento di ledger. Le applicazioni si connettono sempre ai peer quando hanno bisogno di accedere al ledger. L'SDK (Hyperledger Fabric Software Development Kit) lo rende facile ai programmatori: le sue API consentono alle applicazioni di connettersi ai peer, invocare chaincde per generare transazioni, inviare transazioni alla rete che verrà ordinata e impegnata nel libro mastro distribuito e ricevere eventi quando questo processo è completo Attraverso una connessione peer, le applicazioni possono eseguire chaincode per interrogare o aggiornare il libro mastro. Il risultato di una transazione di query ledger viene restituito immediatamente, mentre gli aggiornamenti di ledger implicano un'interazione più complessa tra applicazioni, peer e ordinatori. Analizziamo un po 'più in dettaglio.

![img](HYPERLEDGER/peers.diagram.6_2018-04-25_23-07-14.png)

I peer, in collaborazione con gli ordereer, assicurano che il ledger sia tenuto aggiornato su ogni peer. In questo esempio, l'applicazione A si collega a P1 e richiama il chaincode S1 per interrogare o aggiornare il ledger L1. P1 richiama S1 per generare una risposta di proposta che contiene un risultato di query o un aggiornamento di libro mastro proposto. L'applicazione A riceve la risposta proposta e per le query il processo è ora completo. Per gli aggiornamenti, A crea una transazione da tutte le risposte, che invia a O1 per l'ordine. O1 raccoglie le transazioni dalla rete in blocchi e li distribuisce a tutti i peer, incluso P1. P1 convalida la transazione prima di applicare a L1. Una volta che L1 viene aggiornato, P1 genera un evento, ricevuto da A, per indicare il completamento.

Un peer può restituire immediatamente i risultati di una query a un'applicazione perché tutte le informazioni richieste per soddisfare la query si trovano nella copia locale del ledger del peer. I peer non si consultano con altri peer per restituire una query a un'applicazione. Le applicazioni possono, tuttavia, connettersi a uno o più peer per inviare una query, ad esempio per convalidare un risultato tra più peer o recuperare un risultato più aggiornato da un peer diverso se si sospetta che le informazioni potrebbero essere esaurite. Nel diagramma, puoi vedere che la query del ledger è un semplice processo in tre fasi.

Una transazione di aggiornamento inizia nello stesso modo di una transazione di query, ma ha due passaggi aggiuntivi. Sebbene le applicazioni di aggiornamento della contabilità generale si colleghino anche ai peer per invocare un chaincode, diversamente dal peer-querying delle applicazioni, un singolo peer non può eseguire un aggiornamento del ledger in questo momento, perché gli altri peer devono prima accettare la modifica, un processo chiamato consenso. Pertanto, i peer restituiscono all'applicazione un aggiornamento proposto - uno che questo peer applicherebbe in base all'accordo precedente di altri peer. Il primo passaggio aggiuntivo - quattro - richiede che le applicazioni inviino una serie appropriata di aggiornamenti proposti corrispondenti all'intera rete di peer come transazione per l'impegno nei rispettivi registri. Ciò viene ottenuto dall'applicazione che utilizza un orderer per impacchettare le transazioni in blocchi e distribuirli all'intera rete di peer, dove possono essere verificati prima di essere applicati alla copia locale di ogni peer del ledger. Poiché l'intera elaborazione degli ordini richiede un po' di tempo per essere completata (secondi), l'applicazione viene notificata in modo asincrono, come mostrato nel passaggio cinque.


<a id="org09c414b"></a>

### Peers and Channels

Questi componenti sono in genere nodi peer, nodi orderer e applicazioni e, unendosi a un canale, accettano di riunirsi per condividere collettivamente e gestire copie identiche del ledger per quel canale. Concettualmente puoi pensare che i canali siano simili ai gruppi di amici (sebbene i membri di un canale non debbano essere necessariamente amici!). Una persona potrebbe avere diversi gruppi di amici, con ogni gruppo che ha attività che fanno insieme. Questi gruppi potrebbero essere totalmente separati (un gruppo di amici di lavoro rispetto a un gruppo di amici appassionati), oppure ci può essere un crossover tra di loro. Tuttavia ogni gruppo è una sua entità, con "regole" di un tipo.


<a id="org5050f65"></a>

### Peers and Organizations

Le reti blockchain sono amministrate da una raccolta di organizzazioni piuttosto che da un'unica organizzazione. I peer sono fondamentali per il modo in cui questo tipo di rete distribuita è costruita perché sono di proprietà di - e sono i punti di connessione alla rete - di queste organizzazioni.

![img](HYPERLEDGER/peers.diagram.8_2018-04-26_00-23-48.png)

I Peer in una rete blockchain con più organizzazioni. La rete blockchain è costituita dai peer posseduti e contribuiti dalle diverse organizzazioni. In questo esempio, vediamo quattro organizzazioni che contribuiscono con otto peer per formare una rete. Il canale C collega cinque di questi peer nella rete N - P1, P3, P5, P7 e P8. Gli altri peer di proprietà di queste organizzazioni non sono stati collegati a questo canale, ma in genere sono collegati ad almeno un altro canale. Le applicazioni sviluppate da una particolare organizzazione si collegheranno ai colleghi della propria organizzazione e a quelli di organizzazioni diverse. Di nuovo, per semplicità, un nodo orderer non viene mostrato in questo diagramma.

Si può vedere che (oltre al servizio di ordinazione) non ci sono risorse centralizzate - nell'esempio sopra, la rete, N, non esisterebbe se le organizzazioni non contribuissero con i loro pari. Ciò riflette il fatto che la rete non esiste in alcun senso significativo a meno che fino a quando le organizzazioni non contribuiscano con le risorse che la costituiscono. Inoltre, la rete non dipende da nessuna singola organizzazione: continuerà a esistere finché rimarrà un'organizzazione, indipendentemente dalle altre organizzazioni che possono andare e venire. Questo è il cuore di ciò che significa che una rete deve essere decentralizzata. Le applicazioni in diverse organizzazioni, come nell'esempio sopra, possono o non possono essere le stesse. Questo perché dipende interamente da un'organizzazione come le sue applicazioni elaborano le copie dei peer del ledger. Ciò significa che la logica delle applicazioni e della presentazione può variare da un'organizzazione all'altra anche se i rispettivi peer ospitano esattamente gli stessi dati del libro mastro. Le applicazioni si collegano ai colleghi della propria organizzazione o ai colleghi di un'altra organizzazione, a seconda della natura dell'interazione del registro contabile richiesta. Per le interazioni di tipo ledger-query, le applicazioni si connettono in genere ai peer dell'organizzazione. Per le interazioni di aggiornamento del ledger, vedremo in seguito perché le applicazioni devono connettersi ai peer di tutte le organizzazioni necessarie per approvare l'aggiornamento del ledger.


<a id="org1574e75"></a>

### Peers and Identity

I peer hanno un'identità assegnata loro tramite un certificato digitale da una particolare autorità di certificazione. Ad ogni peer della rete viene assegnato un certificato digitale da un amministratore della sua organizzazione proprietaria.

![img](HYPERLEDGER/peers.diagram.9_2018-04-26_17-39-14.png)

Quando un peer si connette a un canale, il suo certificato digitale identifica la sua organizzazione proprietaria tramite un canale MSP. In questo esempio, P1 e P2 hanno identità emesse da CA1. Il canale C determina da una politica nella sua configurazione di canale che le identità dalla CA1 devono essere associate a Org1 utilizzando ORG1.MSP. Allo stesso modo, P3 e P4 sono identificati da ORG2.MSP come parte di Org2.

Ogni volta che un peer si connette utilizzando un canale a una rete blockchain, una politica nella configurazione del canale utilizza l'identità del peer per determinarne i diritti. La mappatura dell'identità all'organizzazione è fornita da un componente chiamato provider di servizi di appartenenza (MSP) - determina come un peer viene assegnato a un ruolo specifico in una particolare organizzazione e di conseguenza ottiene l'accesso appropriato alle risorse blockchain. Inoltre, un peer può essere posseduto solo da una singola organizzazione ed è quindi associato a un singolo MSP.


<a id="orgb0feec8"></a>

### Peers and Orderers

Abbiamo visto che i peer formano una rete blockchain, che ospita registri e contratti di chaincode che possono essere interrogati e aggiornati da applicazioni peer-connesse. Tuttavia, il meccanismo con cui applicazioni e peer interagiscono tra loro per garantire che il registro di ogni peer sia mantenuto coerente è mediato da nodi speciali chiamati orderer, ed è proprio su questi nodi che ora rivolgiamo la nostra attenzione.

Una transazione di aggiornamento è molto diversa da una transazione di query perché un singolo peer non può, da solo, aggiornare il ledger - richiede il consenso di altri peer della rete. Un peer richiede che altri peer della rete approvino un aggiornamento del ledger prima che possa essere applicato al libro mastro locale di un peer. Questo processo è chiamato consenso e richiede molto più tempo per essere completato rispetto a una query. Ma quando tutti i peer richiesti per approvare la transazione lo fanno, e la transazione è impegnata nel ledger, i peer notificheranno alle applicazioni connesse che il ledger è stato aggiornato.

Nello specifico, le applicazioni che desiderano aggiornare il ledger sono coinvolte in un processo a 3 fasi, che garantisce che tutti i peer di una rete blockchain mantengano i loro registri coerenti tra loro. Nella prima fase, le applicazioni funzionano con un sottoinsieme di peer di approvazione, ognuno dei quali fornisce l'approvazione dell'aggiornamento del ledger proposto all'applicazione, ma non applica l'aggiornamento proposto alla loro copia del ledger. Nella seconda fase, queste approvazioni separate vengono raccolte insieme come transazioni e impacchettate in blocchi. Nella fase finale, questi blocchi vengono distribuiti a ogni peer in cui ogni transazione viene convalidata prima di essere applicata alla copia del ledger di quel peer.

1.  Phase 1: Proposal

    La fase 1 del flusso di lavoro della transazione implica un'interazione tra un'applicazione e un insieme di peer - non coinvolge gli ordereer. La fase 1 riguarda solo un'applicazione che chiede ai peers che sostengono le diverse organizzazioni di accettare i risultati della proposta di invito al chaincode. Per avviare la fase 1, le applicazioni generano una proposta di transazione che inviano a ciascuno dei set di peer richiesti per l'approvazione. Ogni peer quindi esegue in modo indipendente un chaincode utilizzando la proposta di transazione per generare una risposta alla proposta di transazione. Non applica questo aggiornamento al ledger, ma il peer lo firma e ritorna all'applicazione. Una volta che l'applicazione ha ricevuto un numero sufficiente di risposte alle proposte firmate, la prima fase dell'elaborazione della transazione è completa.
    
    Inizialmente, una serie di peer viene scelta dall'applicazione per generare una serie di aggiornamenti di ledger proposti. Quali peer vengono scelti dall'applicazione? Bene, questo dipende dalla politica di approvazione (definita per un chaincode), che definisce l'insieme di organizzazioni che devono approvare una modifica del ledger prima che possa essere accettata dalla rete. Questo è letteralmente ciò che significa raggiungere il consenso - ogni organizzazione che conta deve aver approvato il cambiamento del ledger prima che venga accettato su qualsiasi ledger. Un peer approva una risposta alla proposta aggiungendo la propria firma digitale e firmando l'intero payload utilizzando la sua chiave privata. Questo supporto può essere successivamente utilizzato per dimostrare che il peer di questa organizzazione ha generato una risposta particolare.
    
    La fase 1 termina quando l'applicazione riceve risposte di proposte firmate da sufficienti peers. Notiamo che peer diversi possono restituire risposte di transazione diverse e quindi incoerenti all'applicazione per la stessa proposta di transazione. Potrebbe essere semplicemente che il risultato è stato generato in un momento diverso su diversi peer con registri in stati diversi, nel qual caso un'applicazione può semplicemente richiedere una risposta più aggiornata alla proposta. Meno probabile, ma molto più seriamente, i risultati potrebbero essere diversi perché il chaincode non è deterministico. Il non-determinismo è il nemico dei registri, se si verifica, indica un problema serio con la transazione proposta, poiché i risultati inconsistenti non possono, ovviamente, essere applicati ai ledger. Un singolo peer non può sapere che il risultato della transazione non è deterministico - le risposte di transazione devono essere raccolte per il confronto prima che possa essere rilevato il non-determinismo.

2.  Phase 2: Packaging

    La seconda fase del flusso di lavoro della transazione è la fase di confezionamento. L'ordinante è fondamentale per questo processo: riceve transazioni contenenti risposte di proposte di transazioni approvate da molte applicazioni. Ordina ogni transazione relativa ad altre transazioni e pacchetti di transazioni in blocchi pronti per la distribuzione a tutti i peer connessi all'orderer, inclusi i peer di approvazione originali.
    
    Un ordinatore riceve gli aggiornamenti di ledger proposti contemporaneamente da molte diverse applicazioni nella rete su un particolare canale. Il suo compito è organizzare questi aggiornamenti proposti in una sequenza ben definita e impacchettarli in blocchi per la successiva distribuzione. Questi blocchi diventeranno i blocchi della blockchain! Una volta che un orderer ha generato un blocco della dimensione desiderata, o dopo un tempo massimo trascorso, sarà inviato a tutti i peer ad esso connessi su un particolare canale. Vedremo come questo blocco viene elaborato nella fase 3.
    
    Questo rigoroso ordinamento delle transazioni all'interno dei blocchi rende Hyperledger Fabric un po' diverso da altre blockchain in cui la stessa transazione può essere impacchettata in più blocchi diversi. In Hyperledger Fabric, questo non può accadere - i blocchi generati da una collezione di orderer si dicono definitivi perché una volta che una transazione è stata scritta in un blocco, la sua posizione nel ledger è immutabilmente garantita. In Hyperledger Fabric non può verificarsi un evento disastroso noto come fork di ledger. Una volta che le transazioni sono state acquisite in un blocco, la cronologia non può essere riscritta per quella transazione in un momento futuro.

3.  Phase 3: Validation

    La fase finale del flusso di elaborazione della transazione implica la distribuzione e la successiva convalida dei blocchi dall'ordinante ai peer, in cui possono essere applicati al libro mastro. In particolare, ad ogni peer, ogni transazione all'interno di un blocco viene convalidata per garantire che sia stata sostenuta in modo coerente da tutte le organizzazioni pertinenti prima di essere applicata al libro mastro. Le transazioni non riuscite vengono conservate per la verifica, ma non vengono applicate al ledger
    
    La fase 3 inizia con l'orderer che distribuisce blocchi a tutti i peer ad esso connessi. I peer sono collegati agli orderer sui canali in modo tale che quando viene generato un nuovo blocco, tutti i peer connessi all'orderer ricevino una copia del nuovo blocco. Ogni peer elaborerà questo blocco in modo indipendente, ma esattamente allo stesso modo di ogni altro peer sul canale. In questo modo, vedremo che il ledger può essere mantenuto in modo coerente. Vale anche la pena notare che non tutti i peer devono essere connessi a un orderer - i peer possono collegare a cascata blocchi ad altri peer usando il protocollo di gossip, che può anche elaborarli in modo indipendente.
    
    Alla ricezione di un blocco, un peer processerà ogni transazione nella sequenza in cui appare nel blocco. Per ogni transazione, ciascun peer verificherà che la transazione sia stata approvata dalle organizzazioni richieste in base alla politica di approvazione del chaincode che ha generato la transazione. Ad esempio, alcune transazioni possono solo essere approvate da una singola organizzazione, mentre altre possono richiedere più specializzazioni prima che siano considerate valide. Questo processo di verifica che tutte le organizzazioni rilevanti abbiano generato lo stesso risultato o risultato.
    
    Se una transazione è stata approvata correttamente, il peer tenterà di applicarlo al ledger. Per fare ciò, un peer deve eseguire un controllo di consistenza del ledger per verificare che lo stato corrente del libro mastro sia compatibile con lo stato del ledger quando è stato generato l'aggiornamento proposto. Questo potrebbe non essere sempre possibile, anche quando la transazione è stata pienamente approvata. Ad esempio, un'altra transazione potrebbe aver aggiornato la stessa risorsa nel ledger in modo tale che l'aggiornamento della transazione non è più valido e quindi non può più essere applicato. In questo modo, la copia di ogni peer del ledger viene mantenuta coerente in tutta la rete perché ognuno segue le stesse regole per la convalida.
    
    Dopo che un peer ha validato con successo ogni singola transazione, aggiorna il ledger. Le transazioni non riuscite non vengono applicate al ledger, ma vengono conservate a fini di controllo, così come le transazioni riuscite. Ciò significa che i blocchi peer sono quasi esattamente uguali ai blocchi ricevuti dal committente, ad eccezione di un indicatore valido o non valido su ogni transazione nel blocco.
    
    Infine, ogni volta che un blocco è impegnato per il ledger di un peer, quel peer genera un evento appropriato. Gli eventi di blocco includono il contenuto del blocco completo, mentre gli eventi di transazione blocco includono solo informazioni di riepilogo, ad esempio se ciascuna transazione nel blocco è stata convalidata o invalidata. Gli eventi di Chaincode che l'esecuzione del chaincode ha prodotto possono anche essere pubblicati in questo momento. Le applicazioni possono registrarsi per questi tipi di eventi in modo che possano essere avvisati quando si verificano. Queste notifiche concludono la terza e ultima fase del flusso di elaborazione della transazione.


<a id="org1e87002"></a>

### Orderers and Consensus

L'intero processo del flusso di elaborazione della transazione è chiamato consenso poiché tutti i peer hanno raggiunto un accordo sull'ordine e il contenuto delle transazioni, in un processo mediato dai committenti. Il consenso è un processo a più fasi e le applicazioni vengono notificate solo agli aggiornamenti di Ledger quando il processo è completo, cosa che può accadere in tempi leggermente diversi su peer diversi.


<a id="org807dc4a"></a>

## Ledger

Il ledger è la registrazione sequenziata, resistente alla manomissione di tutte le transizioni di stato. Le transizioni di stato sono il risultato di invocazioni chaincode ("transazioni") presentate dalle parti partecipanti. Ogni transazione genera un insieme di coppie chiave-valore dell'asset che sono impegnate nel ledger come crea, aggiorna o elimina.

Il registro è costituito da una blockchain ('catena') per memorizzare il record immutabile e sequenziato in blocchi, nonché da un database di stato per mantenere lo stato corrente. C'è un ledger per canale. Ogni peer mantiene una copia del libro mastro per ogni canale di cui sono membri.


<a id="orgd279ebe"></a>

### Chain

La catena è un log delle transazioni, strutturato come blocchi hash, in cui ogni blocco contiene una sequenza di N transazioni. L'intestazione del blocco include un hash delle transazioni del blocco, nonché un hash dell'intestazione del blocco precedente. In questo modo, tutte le transazioni sul ledger sono sequenziate e collegate crittograficamente. In altre parole, non è possibile manomettere i dati del registro, senza interrompere i collegamenti hash. L'hash dell'ultimo blocco rappresenta tutte le transazioni precedenti, consentendo di garantire che tutti i peer si trovino in uno stato coerente e affidabile. La catena è memorizzata nel file system peer (locale o storage collegato), supportando in modo efficiente la natura append-only del carico di lavoro della blockchain.


<a id="org2975c24"></a>

### State Database

I dati dello stato corrente del ledger rappresentano i valori più recenti per tutte le chiavi mai incluse nel log delle transazioni della chain. Poiché lo stato corrente rappresenta tutti gli ultimi valori chiave noti al canale, a volte viene indicato come World State. Le chiamate Chaincode eseguono transazioni rispetto ai dati di stato correnti. Per rendere queste interazioni chaincode estremamente efficienti, i valori più recenti di tutte le chiavi sono memorizzati in un database di stato. Il database di stato è semplicemente una vista indicizzata nel log delle transazioni della catena, pertanto può essere rigenerato dalla catena in qualsiasi momento. Il database di stato verrà automaticamente recuperato (o generato se necessario) all'avvio di peer, prima che le transazioni vengano accettate. Le opzioni del database di stato includono LevelDB e CouchDB. LevelDB è il database di stato predefinito incorporato nel processo peer e memorizza i dati del chaincode come coppie chiave-valore. CouchDB è un database di stato opzionale alternativa esterno che fornisce il supporto di query update quando i dati chaincode sono modellati come JSON, permettendo interrogazioni ricche del contenuto JSON.


<a id="orgfcc23e2"></a>

### Transaction Flow

Ad alto livello, il flusso di elaborazione di una transazioni consiste in una proposta di transazione inviata da un client applicativo a peer di approvazione specifici. Gli endorsing peer che approvano e verificano la firma del cliente ed eseguono una funzione di chaincode per simulare la transazione. L'output è il risultati del chaincode, un insieme di versioni chiave-valore che sono state lette nel chaincode (read set) e l'insieme di chiavi / valori che sono stati scritti con il chaincode (set di scrittura). La risposta proposta viene inviata al client insieme a una firma di approvazione.

Il client assembla le convalide in un carico utile della transazione e lo trasmette a un servizio di ordinazione. Il servizio di ordinazione consegna transazioni ordinate come blocchi a tutti i peer di un canale. Prima del commit, i peer convalideranno le transazioni. In primo luogo, controlleranno la politica di approvazione per garantire che l'assegnazione corretta dei peer specificati abbia firmato i risultati e autenticheranno le firme contro il carico utile della transazione. In secondo luogo, i peer eseguiranno un controllo di versione rispetto al set di letture della transazione, per garantire l'integrità dei dati e proteggere da minacce come il double spending. Hyperledger Fabric ha il controllo della concorrenza in base al quale le transazioni vengono eseguite in parallelo (da endorser) per aumentare il throughput, e su commit (da parte di tutti i peer) ogni transazione viene verificata per garantire che nessun'altra transazione abbia modificato i dati letti. In altre parole, garantisce che i dati letti durante l'esecuzione del codice non siano cambiati dal momento dell'esecuzione (approvazione), pertanto i risultati dell'esecuzione sono ancora validi e possono essere impegnati nello state database del ledger. Se i dati letti sono stati modificati da un'altra transazione, la transazione nel blocco viene contrassegnata come non valida e non viene applicata allo state database del ledger. L'applicazione client viene avvisata e può gestire l'errore o riprovare se necessario.
