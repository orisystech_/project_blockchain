#!/bin/bash
#
# Exit on first error, print all commands.
set -e

CHANNEL_NAME="mychannel"
FABRIC_CFG_PATH=$(pwd)

#Start from here
echo -e "\nStopping the previous network (if any)"
docker-compose -f docker-compose.yml down

echo -e "\nCHANNEL NAME:"
echo $CHANNEL_NAME
# If need to re-generate the artifacts, uncomment the following lines and run
#
# cryptogen generate --config=./crypto-config.yaml
# mkdir config
# configtxgen -profile OrgOrdererGenesis -outputBlock ./config/genesis.block
# configtxgen -profile OrgChannel -outputCreateChannelTx ./config/channel.tx -channelID mychannel
# configtxgen -profile OrgChannel -outputAnchorPeersUpdate ./config/Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP
#
# Create and Start the Docker containers for the network
echo -e "\nSetting up the Hyperledger Fabric 1.1 network"
docker-compose -f docker-compose.yml up -d
sleep 15
echo -e "\nNetwork setup completed!!\n"

