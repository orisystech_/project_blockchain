import React, { Component } from 'react';
import {ActivityIndicator, TextInput, TouchableOpacity, StyleSheet, View, Text, Image, Dimensions } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { RootStuck } from '../App';
import {localhost} from '../localhost.js'
import Modal from "react-native-modal";

const window = {height,width} = Dimensions.get('window')

export default class ClientView extends Component {
    static navigationOptions = {
        title: 'Client',
        headerStyle: {
            backgroundColor: 'rgba(44,196,181,0.8)',
        },
        headerTintColor: 'rgba(24,24,26,0.8)',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    constructor(props) {
        super(props);
        this.state = {
            modalOwnerVisible: false,
            modalActivityVisible: false,
            owner: "",
            view: "",
        }
    }

    async fetchQueryByStateAndOwner() {
        this.setState({modalActivityVisible: true})
        let response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/queryByStateAndOwner', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({state: "processed",owner: this.state.owner})
        })
        let responseProductProcessed = await response.json();
        console.log(" risultato processed product " + JSON.stringify(responseProductProcessed))
        response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/queryByStateAndOwner', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({state: "shipped",owner: this.state.owner})
        })
        let responseProductShipped = await response.json();
        console.log(" risultato shipped product " + JSON.stringify(responseProductShipped))

        response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/queryByStateAndOwner', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({state: "in_processing",owner: this.state.owner})
        })
        let responseProductInProcessing = await response.json();
        console.log(" risultato shipped product " + JSON.stringify(responseProductInProcessing))

        await this.setState({modalOwnerVisible:false,view: "", owner: "",modalActivityVisible: false})
        productProcessedAndShipped = responseProductProcessed.concat(responseProductShipped)

        return productProcessedAndShipped.concat(responseProductInProcessing)

    }

    async fetchQueryByOwner() {
        await this.setState({modalActivityVisible: true})
        response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/queryByOwner', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({state: null,owner: this.state.owner})
        })
        let responseProductByOwner = await response.json();
        console.log(" risultato shipped product " + JSON.stringify(responseProductByOwner))

        await this.setState({modalOwnerVisible:false,view: "", owner: "",modalActivityVisible: false})
        return responseProductByOwner
    }

    fetchAndNavigate(view) {
        if (view == "tracking") { 
            this.fetchQueryByStateAndOwner().then((data) => this.props.navigation.navigate('Tracking_client',{data:data}))
        }
        else if (view == "myorder"){
            this.fetchQueryByOwner().then((data) => this.props.navigation.navigate('MyOrder',{data:data}))
        }
    }

    renderOwnerModal(view) {
        return (
            <Modal
                isVisible={this.state.modalOwnerVisible}
                transparent={true}
                backgroundOpacity={0}
                animationType='fade'
                onBackdropPress={() => this.setState({modalOwnerVisible: false})}
            >
                <View
                    style={styles.modalOwner}
                >
                    <Text
                        adjustsFontSizeToFit={true}
                        minimumFontScale={0.8}
                        style={{ alignSelf: 'center',color: 'black',paddingTop:10 }}>
                        Insert costumer's name
                    </Text>
                    <TextInput
                        autoCapitalize='words'
                        placeholderTextColor='black'
                        clearButtonMode='while-editing'
                        style={{marginTop:30,height:40,width:window.width*0.4,borderColor: 'gray', backgroundColor: 'white',alignItems:'center',justifyContent:'center',borderWidth: 1}}
                        onChangeText={(owner) => this.setState({owner})}
                        value={this.state.owner}
                    />
                    <TouchableOpacity style={styles.button} onPress={ () => this.fetchAndNavigate(view)}>
                        {this.state.modalActivityVisible == false ?
                             <Text style={styles.textButton}> Ok </Text>
                         :
                             <ActivityIndicator size="large" color="black" />
                         
                        }

                    </TouchableOpacity>
                </View>
            </Modal>

        )
    }


    render () {
        const screen = this.props;
        
        return (

            <View style={styles.container}>

                <View style={{flexDirection:'row', justifyContent:'space-between', marginHorizontal: 8}}>
                    <TouchableOpacity style={styles.square} onPress={() => this.props.navigation.navigate('Product_client')}>
                        <Text style={styles.text}> Prodotti </Text>
                        <Image style={styles.img} source={require('../icon/cart.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.square} onPress={() => this.props.navigation.navigate('Transactions')}>
                        <Text style={styles.text}> Block Explorer </Text>
                        <Image style={styles.img} source={require('../icon/blockExplorer.png')}/>
                    </TouchableOpacity>
                </View>   
                <View style={{flexDirection:'row', justifyContent:'space-between', marginHorizontal: 8}}>
                    <TouchableOpacity style={styles.square} onPress={() => this.setState({modalOwnerVisible: true,view:"myorder"})}>
                        <Text style={styles.text}> My Order </Text>
                        <Image style={styles.img} source={require('../icon/order.png')}/> 
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.square} onPress={() => this.setState({modalOwnerVisible:true,view:"tracking"})}>
                        <Text style={styles.text}> Tracking </Text>
                        <Image style={styles.img} source={require('../icon/tracking.png')}/>
                    </TouchableOpacity>
                </View>
                {this.renderOwnerModal(this.state.view)}
            </View>
        );  
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(31,32,32,0.8)',
        justifyContent: 'center'
    },
    modalOwner: {
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        top: window.height*0.25,
        left: window.width*0.05,
        //borderRadius: 40,
        height: window.height*0.23,
        width: window.width*0.8,
        backgroundColor: 'white'
    },
    button: {
        height: 40,
        width: 180,
        borderRadius: 50,
        elevation: 10,
        backgroundColor: '#ededed',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5
    },
    square: {
        width: 170,
        height: 170,
        borderRadius: 10,
        marginBottom: 15,
        backgroundColor: 'rgba(255,255,255,0.8)',
        alignItems: 'center',
        overflow: 'hidden'
        
    },
    img: {
        height: 140,
        width: 140,
        resizeMode: 'center',
        alignSelf: 'center'
    },
    text:{
        backgroundColor: 'rgba(165,166,173,1)',
        width: 180,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    }
});
