package com.hyperledgerDemo.config;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.TransactionException;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.hyperledgerDemo.config.network.CreateChannel;

@Configuration
@ComponentScan("com.hyperledgerDemo")
@EnableWebMvc
public class WebServerConfig extends WebMvcConfigurerAdapter {

    private static final Logger log = Logger.getLogger(WebServerConfig.class);

    
    @PostConstruct
    private void InitializeNetwork() throws InvalidArgumentException,TransactionException,Exception  {
        CreateChannel.createChannelNetwork();
        CreateChannel.deployAndInstantiateChaincode();
    }

}

