import React, { Component } from 'react';
import { View,Text,Platform,StatusBar } from 'react-native';
import { StackNavigator } from 'react-navigation';
// components
import HomeScreen from './view/HomeScreen';
import EcommerceView from './view/EcommerceView';
import CourierView from './view/CourierView';
import ClientView from './view/ClientView';
import BlockExplorer_cl from './view/BlockExplorer_cl';
import Product_cl from './view/Product_cl';
import Tracking_cl from './view/Tracking_cl';
import TransactionView from './view/TransactionView';
import ProcessOrder from './view/ProcessOrder.js';
import ProcessSpedition from './view/ProcessSpedition.js';
import MyOrder from './view/MyOrder.js';

export default class App extends Component {
    
    render() {
        return (
                <RootStack/>
        )
    }
}

const RootStack = StackNavigator ({
    Home: {
        screen: HomeScreen,
    },
    Ecommerce: {
        screen: EcommerceView,
        navigationOptions: {
            header: null,
        }
    },
    Courier: {
        screen: CourierView,
        navigationOptions: {
            header: null,
        }
    },
    Client: {
        screen: ClientView,
        navigationOptions: {
            header: null,
        }
    },
    BlockExplorer_client: {
        screen: BlockExplorer_cl,
        navigationOptions: {
            header: null, 
        }
    },
    Product_client: {
        screen: Product_cl,
        navigationOptions: {
            header: null,
        }
    },
    Tracking_client: {
        screen: Tracking_cl,
        navigationOptions: {
            header: null,
        }
    },
    MyOrder: {
        screen: MyOrder,
        navigationOption: {
            header: null,
        }
    },
    Transactions: {
        screen: TransactionView,   
        navigationOptions: {
            header: null,
        }
    },
    ProcessedSpedition: {
        screen: ProcessSpedition,
        navigationOptions: {
            header: null,
        }
    },
    ProcessedOrder: {
        screen: ProcessOrder,
        navigationOptions: {
            header: null,
        }
    }
},
                                  {
                                      headerMode: 'screen',
                                  },
                                  {
                                      initialRouteName: 'Home',
                                      cardStyle: {
                                          paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
                                      }
                                  },
);

