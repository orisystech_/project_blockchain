import React, { Component } from 'react';
import { StyleSheet, View, Text, AsyncStorage,FlatList, Modal, Dimensions, TouchableOpacity, Image } from 'react-native';
import { StackNavigator } from 'react-navigation';

//Constants
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
const cardHeight = screenHeight * 0.20;
const cardWidth = screenWidth * 0.95;

export default class TransactionView extends Component {
    static navigationOptions = {
        title: 'Transactions',
        headerStyle: {
            backgroundColor: 'rgba(44,196,181,0.8)',
        },
        headerTintColor: 'rgba(24,24,26,0.8)',
        headerTitleStyle: {
        },
    };

    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this)
        //this.data = this.props.navigation.state.params.data
        this.state = {
            data: []
        }
    }

    componentDidMount(){
       this.getTransactions() 
    }

    async getTransactions() {
        const value = await AsyncStorage.getItem('transactions');
        if (value != null) {
            this.setState({data: JSON.parse(value)})
            console.log("transaction ids " + JSON.stringify(this.state.data))
        }
    }

    _renderItem({item,index}) { 
        return(
                <View style={styles.txContainer}>
                    <View style={styles.txSquare}>
                        <Text style={{color:'#fff', fontWeight:'bold', fontSize: 15 }}> TxId n°{index}: </Text>
                    </View>
                    <View >
                        <Text style={{ fontSize: 15}}> {item} </Text>
                    </View>
                </View>
        )
    }

    _keyExtractor(item,index) {
        return item
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data= {this.state.data}
                    renderItem= {this._renderItem}
                    keyExtractor= {this._keyExtractor}
                    inverted={true}
                >
                </FlatList>
            </View>
        );
    }
}

const styles = StyleSheet.create ({
    container: {
        //flex:1,
        //transform: [
        //    { scaleY: -1 },
        //],
        backgroundColor: '#4f5051',
        height: screenHeight,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txContainer: {
        //flex:1,
        //transform: [
        //    { scaleY: -1 },
        //],
        height: cardHeight,
        width: cardWidth,
        marginVertical: 10,
        borderRadius: 10,
        flexDirection: 'column',
        backgroundColor: '#cccccc'
    },
    txSquare: {
        height: 25,
        backgroundColor: '#1b4ee8',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txInfoSquare: {
        height: 30,
        width: 160,
        marginRight: 45,
        borderRadius: 4,
        backgroundColor: '#1b4ee8',
        marginTop: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    txText: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 10,
        color: '#1b4ee8'
    },
    button: {
        height: 35,
        width: 130,
        borderRadius: 50,
        backgroundColor: '#ededed',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15
    },
    close: {
        backgroundColor: '#cccccc',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 2,
        marginTop: 2,
        marginBottom: 10,
        height: 40,
        width: 40,
        borderRadius: 2
    },
    textButton: {
        color: '#0c1660',
        fontSize: 18,
        fontWeight: 'bold'
    }
});
