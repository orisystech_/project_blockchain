import React, { Component } from 'react';
import { View, Text, Image,TextInput, AsyncStorage, StyleSheet, TouchableOpacity, Dimensions, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { ENTRIES2 } from './entries';
import { RootStuck } from '../App';
import Modal from "react-native-modal";
import {localhost} from '../localhost.js'

//Constants
const IS_IOS = Platform.OS === 'ios';
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
const slideHeight = screenHeight * 0.35;
const slideWidth = screenWidth * 0.75;


const window = {height,width} = Dimensions.get('window')

export default class SliderEntry_product extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            costumerName: ''
        }
    }
    static propTypes = {
        data: PropTypes.object.isRequired,
    };

    async fetchOrderProduct() {
        const { data: { category,title, price }} = this.props;
        let response = await fetch( 'http://'+localhost+':8080/hyperledgerDemo/orderProduct', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({pName: this.props.data.title,state: 'in_processing',location:" ",type: this.props.data.category,owner:this.state.costumerName})
        })
        let responseText = await response.text();
        console.log(" risultato " + responseText)
        const value = await AsyncStorage.getItem('transactions');
        if (value != null) {
            console.log("add " + responseText + " in storage")
            transactionsId = JSON.parse(value)
            transactionsId.push(responseText)
        } else {
            console.log("create array transactionsId from " + responseText + " in storage")
            transactionsId = [responseText]
        }

        await AsyncStorage.setItem('transactions', JSON.stringify(transactionsId));
        this.setState({modalVisible: false,costumerName: ''})
    }

    renderModal() {
            return (
                <Modal
                    isVisible={this.state.modalVisible}
                    transparent={true}
                    backgroundOpacity={0}
                    animationType='fade'
                    onBackdropPress={() => this.setState({modalVisible: false})}
                >
                    <View
                        style={styles.container}
                    >
                        <Text
                            adjustsFontSizeToFit={true}
                            minimumFontScale={0.8}
                            style={{ alignSelf: 'center',color: 'black',paddingTop:10 }}>
                            Insert costumer's name
                        </Text>
                        <TextInput
                            autoCapitalize='words'
                            placeholderTextColor='black'
                            clearButtonMode='while-editing'
                            style={{marginTop:30,height:40,width:window.width*0.4,borderColor: 'gray', backgroundColor: 'white',alignItems:'center',justifyContent:'center',borderWidth: 1}}
                            onChangeText={(costumerName) => this.setState({costumerName})}
                            value={this.state.costumerName}
                        />
                        <TouchableOpacity style={styles.button} onPress={ () => this.fetchOrderProduct()}>
                            <Text style={styles.textButton}> Order </Text> 
                        </TouchableOpacity>
                    </View>
                </Modal>

            )
    }

   get image() {
        const { data: {illustration}} = this.props;
        return (
            <Image source={illustration} style={styles.image_product}>
            </Image>
        );
    }

    render() {
        const {category, data: { title, price }} = this.props;
        
        const uppercaseTitle = title ? (
            <Text style={[styles.title_product]}> 
                { title.toUpperCase() }
            </Text>
        ) : false;
        
        return (
        <View style={styles.slideInnerContainer_product}>
            <TouchableOpacity onPress={() => this.setState({modalVisible:true})}style={{justifyContent:'center', alignItems:'center'}}>          
                {this.image}
            </TouchableOpacity>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                {uppercaseTitle} <Text style={styles.price}> {price} </Text>
            </View>
            {this.renderModal()}
        </View>
        );
    }
}    


const styles = StyleSheet.create ({
    container: {
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        top: window.height*0.25,
        left: window.width*0.05,
        //borderRadius: 40,
        height: window.height*0.23,
        width: window.width*0.8,
        backgroundColor: 'white'
    },
    textButton: {
        color: '#0c1660',
        fontSize: 18,
        fontWeight: 'bold'
    },
    button: {
        height: 40,
        width: 180,
        borderRadius: 50,
        elevation: 10,
        backgroundColor: '#ededed',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5
    },
    image_product: {
        resizeMode: 'center',
        height: 200,
        width: 200
    },
    title_product: {
        color: '#000000',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'left'
    },
    slideInnerContainer_product: {
        backgroundColor: '#fff',
        width: slideWidth,
        height: slideHeight,
        paddingHorizontal: 5
          },
    imageContainer_product: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(224,227,220,0.8)'
    },
    price: {
        textAlign: 'right',
        fontSize: 17,
        fontWeight: 'bold',
        backgroundColor: '#fff319'
    }
});
