import React, { Component } from 'react';
import { Platform, StyleSheet, View, Text, ScrollView, Dimensions } from 'react-native';
import { StackNavigator } from 'react-navigation';
import Carousel from 'react-native-snap-carousel';
import SliderEntry_product from '../components/SliderEntry_product';
import { product, ENTRIES1} from '../components/entries';

const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 0;
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
const containerHeight = screenHeight * 0.45;
const containerWidth = screenWidth * 0.95;
const itemWidth = screenWidth * 0.75;
const sliderWidth = containerWidth;


export default class Product_cl extends Component {
    
          constructor (props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM
        };
      }

    _renderItem ({item,index}) {
        return (
            <SliderEntry_product  data={item}/>
        )
    }

    _carousel(data,index) {
        return (
            <View key={index} style={styles.carouselContainer}>
                <View style={styles.carouselInnerContainer}>
                    <Text style={styles.category}>
                        {data.category}
                    </Text>
                </View>
                    <Carousel
                        ref={c => this._slider1Ref = c}
                        data={data.data}
                        renderItem={this._renderItem}
                        itemWidth={itemWidth}
                        sliderWidth={sliderWidth}
                        firstItem={SLIDER_1_FIRST_ITEM}
                        onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }>
                    </Carousel>
            </View>
        );
    }
    
    render () {

        return (
            <View style={{paddingTop: 20, backgroundColor:'#4f5051'}}>
            <ScrollView style={styles.container}>
            {product.map((item,index) => {
                return this._carousel(item,index,)
            })}
            </ScrollView>  
            </View>

        );
    }
}

const styles = StyleSheet.create ({
    container: {
        backgroundColor: '#4f5051'
    },
    text: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    },
    category: {
        fontWeight: 'bold',
        color: '#4f5051',
        fontSize: 30,
        textAlign: 'left',
        marginLeft: 8,
        marginBottom: 2
    },
    carouselContainer: {
        height: containerHeight,
        width: containerWidth,
        backgroundColor: '#cccccc',
        marginVertical: 10,
        marginHorizontal: 10,
        borderRadius: 8
    },
    carouselInnerContainer: {
        borderBottomWidth: 2,
        borderColor: 'rgba(44,196,181,0.8)',
        marginVertical: 8
    }
});
 
