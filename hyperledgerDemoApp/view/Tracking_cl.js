import React, { Component } from 'react';
import { FlatList,StyleSheet, Image, View, ScrollView, Text, TouchableOpacity, Dimensions } from 'react-native';
import { StackNavigator } from 'react-navigation';
import {product} from '../components/entries.js'

//Constants
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
const cardHeight = screenHeight * 0.40;
const cardWidth = screenWidth * 0.95;

export default class Tracking_cl extends Component {
    static navigationOptions = {
        title: 'Tracking',
        headerStyle: {
            backgroundColor: 'rgba(165,166,173,0.8)',
        },
        headerTintColor: 'rgba(24,24,26,0.8)',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this)
        this._keyExtractor = this._keyExtractor.bind(this)
        this.data = this.props.navigation.state.params.data
    }

    _renderItem({item}) {
        return (
            <View style={styles.detailView}>
                <Text style={styles.textDettagliOrdine}> Order Detail</Text>
                <View style={{flexDirection:'column'}}>
                    <View style={styles.lineStyle}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:18, fontWeight:'bold', color:'#666666', marginBottom:5}}> Order n°: </Text>
                            <Text style={{fontSize:18}}> {item.Key} </Text>
                        </View>
                    </View>
                </View>
                <Text style={styles.textDettagliOrdine}>Shipping Detail</Text>
                <View style={styles.lineMiddleStyle}>
                    <View style={{flexDirection:'row'}}>
                        {this.image(item.Record["type"],item.Record["pname"])}
                        <View style={{flexDirection:'column'}}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontSize: 18, marginTop: 10, marginBottom: 15, fontWeight:'bold'}}> {item.Record["pname"]}</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontSize:18, fontWeight:'bold', color:'#666666', marginBottom:5}}> Location: {item.Record["location"]} </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    image(category,productName) {
        for (i=0;i<product.length;i++) {
            if (product[i].category==category) {
                let data = product[i].data
                for (j=0;j<data.length;j++) {
                    if (data[j].title == productName) {
                        return (
                            <Image source={data[j].illustration} style={styles.image_product}>
                            </Image>
                        );
                    }
                }
            }
        }
    }


    _keyExtractor(item,index) {
        return item.Key
    }

    render() {
        console.log("data " + JSON.stringify(this.data))
        return (
            <ScrollView style={styles.container}>
                <View style={{alignItems:'center'}}>
                    <FlatList
                        data={this.data}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                    >
                    </FlatList>
                </View>
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: 'rgba(31,32,32,0.8)'
    },
    detailView: {
        backgroundColor: '#cccccc',
        borderRadius: 8,
        height: cardHeight,
        width: cardWidth,
        marginTop: 20,
        marginBottom: 15
    },
    textDettagliOrdine: {
        fontWeight: 'bold',
        fontSize: 20 ,
        textAlign: 'center',
        marginBottom: 15,
        marginTop: 10
    },
    lineStyle: {
        borderColor: '#efefef',
        borderRadius: 3,
        borderBottomWidth: 2,
        borderRightWidth: 2, 
        marginLeft: 5,
        width: cardWidth * 0.8
    },
    image_product: {
        resizeMode: 'center',
        height: 120,
        width: 120,
        marginLeft: 5,
        marginTop:10
    },
    lineMiddleStyle: {
        borderColor: '#efefef',
        borderRadius: 3,
        borderBottomWidth: 2,
        borderRightWidth: 2, 
        marginLeft: 5,
        width: cardWidth * 0.90,
        height: cardHeight * 0.50
    }

});
