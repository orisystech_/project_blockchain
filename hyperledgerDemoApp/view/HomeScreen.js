import React, { Component } from 'react';
import { Platform, View, ScrollView, Text, StatusBar, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../styles/SliderEntry.style';
import SliderEntry from '../components/SliderEntry';
import styles, { colors } from '../styles/index.style';
import { ENTRIES1 } from '../components/entries';
import { StackNavigator } from 'react-navigation';

const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 1;

export default class HomeScreen extends Component {
    
    static navigationOptions = { 
        header: null 
    }

    constructor(props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM
        };
        this.navigation = this.props.navigation
    }
    
        _renderItemWithParallax ({item, index}, parallaxProps) {
        return (
            <SliderEntry
              data={item}
              parallax={true}
              parallaxProps={parallaxProps}
              navigation={this.props.navigation}
              screen={item.screenTitle}
            />
        );
    }

    mainExample (title) {
        const { slider1ActiveSlide } = this.state;

        return (
            <View >
                <Carousel
                  ref={c => this._slider1Ref = c}
                  data={ENTRIES1}
                  renderItem={this._renderItemWithParallax.bind(this)}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  hasParallaxImages={true}
                  firstItem={SLIDER_1_FIRST_ITEM}
                  inactiveSlideScale={1}
                  inactiveSlideOpacity={0.5}
                  onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                />
                <Pagination
                  dotsLength={ENTRIES1.length}
                  activeDotIndex={slider1ActiveSlide}
                  dotColor={'rgba(32,185,207,0.8)'}
                  dotStyle={styles.paginationDot}
                  inactiveDotColor={colors.black}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                  carouselRef={this._slider1Ref}
                  tappableDots={!!this._slider1Ref}
                />
            </View>
        );
    }

render () {
    const example1 = this.mainExample('DEMO');

    return (
            <View style={styles.container}>
                <StatusBar
                  translucent={true}
                  backgroundColor={'rgba(48,50,49,0.8)'}
                  barStyle={'dark-content'}
                />
                <View style={styles.containerLogo}>
                <Image style={styles.img} source={require('../icon/logoHl.png')}/>
                </View>
                <ScrollView
                  scrollEventThrottle={200}
                  directionalLockEnabled={true}
                >
                    { example1 }
                </ScrollView>
            </View>
        );
    }
}


