package com.hyperledgerDemo.service;

import org.springframework.stereotype.Service;

@Service
public class changeOrderService {

    private String fnc;

    private String key;

    private String argument;

    /**
     * @return the fnc
     */
    public String getFnc() {
        return fnc;
    }

    /**
     * @param fnc the fnc to set
     */
    public void setFnc(String fnc) {
        this.fnc = fnc;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the argument
     */
    public String getArgument() {
        return argument;
    }

    /**
     * @param argument the argument to set
     */
    public void setArgument(String argument) {
        this.argument = argument;
    }
}
