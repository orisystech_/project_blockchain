import { StyleSheet } from 'react-native';

export const colors = {
    black: '#1a1917',
    white: '#FDF7FF',
    gray: '#888888',
};

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(31,32,32,0.8)',
    },
    containerLogo: {
        height: 80,
        width: window.width,
        paddingTop: 25,
    },
    gradient: {
        ...StyleSheet.absoluteFillObject
    },
    scrollview: {
        flex: 1
     },
    title: {
        paddingHorizontal: 30,
        backgroundColor: 'rgba(32,185,207,0.8)' ,
        color: 'rgba(237,255,229,0.8)',
        fontSize: 28,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    titleDark: {
        color: colors.black
    },
    paginationDot: {
        width: 10,
        height: 10,
        borderRadius: 4,
        marginHorizontal: 8
    },
    img:{
        resizeMode: 'contain',
        height: 50,
        width: window.width,

    }
});
