import React, { Component } from 'react';
import { AppRegistry, View, Image } from 'react-native';
import { StackNavigator } from 'react-navigation'
import { RootStack } from '../App'

export const ENTRIES1 = [
    {
        title: 'E-commerce',
        illustration: require('../icon/e-commerce.png'),
        screenTitle: "Ecommerce"
    },
    {
        title: 'Courier',
        illustration: require('../icon/shipping.png'),
        screenTitle: "Courier"

    },
    {
        title: 'Client',
        illustration: require('../icon/client.png'),
        screenTitle: "Client"
    },  
];

const office = {
    category: 'Office',
    data: [
        {
            title: 'Computer',
            price: '299€',
            category: 'Office',
            illustration: require('../icon/laptop.jpeg')
        },
        {
            title: 'Monitor',
            price: '99€',
            category: 'Office',
            illustration: require('../icon/monitor.jpg')
        }
    ]
};

const home = {
    category: 'Home',
    data: [
        {
            title: 'Table',
            price: '50€',
            category: 'Home',
            illustration: require('../icon/table.jpeg')
        },
        {
            title: 'Chair',
            price: '39€',
            category: 'Home',
            illustration: require('../icon/chair.jpg')
        }
    ]
};

const mobile = {
    category: 'Mobile',
    data: [
        {
            title: 'Iphone',
            price: '800€',
            category: 'Mobile',
            illustration: require('../icon/iphone.png')
        },
        {
            title: 'Samsung',
            price: '700€',
            category: 'Mobile',
            illustration: require('../icon/samsung.png')
        }
    ]
};

export const product = [office,home,mobile];

