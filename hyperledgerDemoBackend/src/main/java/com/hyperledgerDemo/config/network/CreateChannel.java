package com.hyperledgerDemo.config.network;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.ChannelConfiguration;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.TransactionRequest.Type;
import org.hyperledger.fabric.sdk.security.CryptoSuite;

import com.hyperledgerDemo.config.Config;
import com.hyperledgerDemo.config.client.ChannelClient;
import com.hyperledgerDemo.config.client.FabricClient;
import com.hyperledgerDemo.user.UserContext;
import com.hyperledgerDemo.util.Util;

public class CreateChannel {

    public static void createChannelNetwork() {
        try {
            CryptoSuite.Factory.getCryptoSuite();
            Util.cleanUp();
            // Construct Channel
            UserContext org1Admin = new UserContext();
            File pkFolder1 = new File(Config.ORG1_USR_ADMIN_PK);
            File[] pkFiles1 = pkFolder1.listFiles();
            File certFolder1 = new File(Config.ORG1_USR_ADMIN_CERT);
            File[] certFiles1 = certFolder1.listFiles();
            String[] listFileString = certFolder1.list();
            Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,"lunghezza file %d " + pkFiles1[0].getName());
            Enrollment enrollOrg1Admin = Util.getEnrollment(Config.ORG1_USR_ADMIN_PK, pkFiles1[0].getName(), Config.ORG1_USR_ADMIN_CERT, certFiles1[0].getName());
            org1Admin.setEnrollment(enrollOrg1Admin);
            org1Admin.setMspId(Config.ORG1_MSP);
            org1Admin.setName(Config.ADMIN);

            FabricClient fabClient = new FabricClient(org1Admin);

            // Create a new channel
            Orderer orderer = fabClient.getInstance().newOrderer(Config.ORDERER_NAME, Config.ORDERER_URL);
            ChannelConfiguration channelConfiguration = new ChannelConfiguration(new File(Config.CHANNEL_CONFIG_PATH));

            byte[] channelConfigurationSignatures = fabClient.getInstance()
                .getChannelConfigurationSignature(channelConfiguration, org1Admin);

            Channel mychannel = fabClient.getInstance().newChannel(Config.CHANNEL_NAME, orderer, channelConfiguration,
                                                                   channelConfigurationSignatures);

            Peer peer0_org1 = fabClient.getInstance().newPeer(Config.ORG1_PEER_0, Config.ORG1_PEER_0_URL);
            Peer peer1_org1 = fabClient.getInstance().newPeer(Config.ORG1_PEER_1, Config.ORG1_PEER_1_URL);
            Peer peer2_org1 = fabClient.getInstance().newPeer(Config.ORG1_PEER_2, Config.ORG1_PEER_2_URL);

            mychannel.joinPeer(peer0_org1);
            mychannel.joinPeer(peer1_org1);
            mychannel.joinPeer(peer2_org1);

            mychannel.addOrderer(orderer);

            mychannel.initialize();

            Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO, "Channel created "+mychannel.getName());
            Collection peers = mychannel.getPeers();
            Iterator peerIter = peers.iterator();
            while (peerIter.hasNext())
                {
                    Peer pr = (Peer) peerIter.next();
                    Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,pr.getName()+ " at " + pr.getUrl());
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deployAndInstantiateChaincode() {
        try {
            CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
			
            UserContext org1Admin = new UserContext();
            File pkFolder1 = new File(Config.ORG1_USR_ADMIN_PK);
            File[] pkFiles1 = pkFolder1.listFiles();
            File certFolder = new File(Config.ORG1_USR_ADMIN_CERT);
            File[] certFiles = certFolder.listFiles();
            Enrollment enrollOrg1Admin = Util.getEnrollment(Config.ORG1_USR_ADMIN_PK, pkFiles1[0].getName(),
                                                            Config.ORG1_USR_ADMIN_CERT, certFiles[0].getName());
            org1Admin.setEnrollment(enrollOrg1Admin);
            org1Admin.setMspId("Org1MSP");
            org1Admin.setName("admin");

            FabricClient fabClient = new FabricClient(org1Admin);

            Channel mychannel = fabClient.getInstance().newChannel(Config.CHANNEL_NAME);
            Orderer orderer = fabClient.getInstance().newOrderer(Config.ORDERER_NAME, Config.ORDERER_URL);
            Peer peer0_org1 = fabClient.getInstance().newPeer(Config.ORG1_PEER_0, Config.ORG1_PEER_0_URL);
            Peer peer1_org1 = fabClient.getInstance().newPeer(Config.ORG1_PEER_1, Config.ORG1_PEER_1_URL);
            Peer peer2_org1 = fabClient.getInstance().newPeer(Config.ORG1_PEER_2, Config.ORG1_PEER_2_URL);
            mychannel.addOrderer(orderer);
            mychannel.addPeer(peer0_org1);
            mychannel.addPeer(peer1_org1);
            mychannel.addPeer(peer2_org1);
            mychannel.initialize();

            List<Peer> org1Peers = new ArrayList<Peer>();
            org1Peers.add(peer0_org1);
            org1Peers.add(peer1_org1);
            org1Peers.add(peer2_org1);
			
            Collection<ProposalResponse> response = fabClient.deployChainCode(Config.CHAINCODE_1_NAME,
                                                                              Config.CHAINCODE_1_PATH, Config.CHAINCODE_ROOT_DIR, Type.GO_LANG.toString(),
                                                                              Config.CHAINCODE_1_VERSION, org1Peers);
			
			
            for (ProposalResponse res : response) {
                Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,
                                                                                 Config.CHAINCODE_1_NAME + "- Chain code deployment " + res.getStatus());
            }

			
            ChannelClient channelClient = new ChannelClient(mychannel.getName(), mychannel, fabClient);

            String[] arguments = { "" };
            response = channelClient.instantiateChainCode(Config.CHAINCODE_1_NAME, Config.CHAINCODE_1_VERSION,
                                                          Config.CHAINCODE_1_PATH, Type.GO_LANG.toString(), "init", arguments, null);

            for (ProposalResponse res : response) {
                Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,
                                                                                 Config.CHAINCODE_1_NAME + "- Chain code instantiation " + res.getStatus());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
