import React, { Component } from 'react';
import { FlatList, TouchableOpacity, ActivityIndicator, StyleSheet, View, Text, Dimensions, Platform } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { dataList } from '../components/data.js'; 
import TransactionView from './TransactionView.js';

//Constants
const IS_IOS = Platform.OS === 'ios';
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
const cardHeight = screenHeight * 0.32;
const cardWidth = screenWidth * 0.95;
 
export default class BlockExplorer_cl extends Component {
    static navigationOptions = {
        title: 'Block Explorer',
        headerStyle: {
            backgroundColor: 'rgba(44,196,181,0.8)',
        },
        headerTintColor: 'rgba(24,24,26,0.8)',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    
    constructor(props){
        super(props);

        this.state = {
            data: dataList
        };
        this.navigation = this.props.navigation
    }

    _renderItem({item,index}) {
        return(
            <View style={styles.background}>
                <View style={styles.cardStyle}>
                    <View style={{flexDirection:'row'}}>
                        <View style={styles.blockSquare}>
                            <Text style={{fontWeight:'bold', color:'#fff', textAlign: 'center'}}>{item['blockNumber']}</Text>
                        </View>
                        <View style={{flex: 1, right: 6, justifyContent:'center', alignItems:'center'}}>
                            <View style={styles.hashSquare}>
                                <Text style={{fontSize: 20, fontWeight: 'bold', color:'#fff'}}> Hash: </Text>
                            </View>
                        </View> 
                    </View>
                    <View style={{justifyContent:'center', alignItems:'center', marginLeft: 6}}>
                        <Text style={{textAlign:'center', width:(cardWidth)*0.80, marginTop: 6, fontSize: 14, fontWeight: 'bold'}}>{item['data']}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{fontSize: 16, marginLeft: 6, fontWeight: 'bold', marginTop: 12, color: '#1b4ee8'}}> Prev Hash: </Text>
                        <Text style={{textAlign:'center', width:(cardWidth)*0.60, marginTop: 12, marginLeft: 12, fontSize: 12}}>{item['prevHash']}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{fontSize: 16, marginLeft: 6, fontWeight: 'bold', marginTop: 12, color:'#1b4ee8'}}> Num Tx: </Text>
                        <Text style={{marginTop: 13, marginLeft: 10, fontSize: 16, fontWeight:'bold'}}>{item['numTx']}</Text>
                    </View>
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                        <TouchableOpacity style={styles.button} onPress={ () => this.props.navigation.navigate("Transactions",{data: item.tx})}>
                            <Text style={styles.textButton}> See all TX </Text> 
                        </TouchableOpacity>
                    </View>    
                </View>
            </View>
        )
    }

    _keyExtractor(item,index) {
        return item.blockNumber
    }

    render() {
        return(
            <View style={{backgroundColor:'#4f5051'}}>
                <View style={{paddingTop: 20}}>    
                    <FlatList
                        data= {this.state.data}
                        inverted={true}
                        renderItem= {this._renderItem.bind(this)}
                        keyExtractor= {this._keyExtractor.bind(this)}
                    >
                    </FlatList>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create ({
    background: {
        backgroundColor: '#4f5051',
        justifyContent: 'center',
        alignItems:'center'
    },
    cardStyle: {
        backgroundColor:'#cccccc',
        height: cardHeight,
        width: cardWidth,
        marginVertical: 15,
        borderRadius: 10
    },
    blockSquare: {
        borderRadius: 2,
        height: 30,
        width: 30,
        marginTop: 6,
        marginLeft: 6,
        backgroundColor: '#ed5923',
        justifyContent: 'center'
    },
    hashSquare: {
        borderRadius: 4, 
        height: 30,
        width: 110,
        marginTop: 6,
        backgroundColor: '#1b4ee8',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        height: 40,
        width: 180,
        borderRadius: 50,
        elevation: 10,
        backgroundColor: '#ededed',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 5
    },
    textButton: {
        color: '#0c1660',
        fontSize: 18,
        fontWeight: 'bold'
    }
});
