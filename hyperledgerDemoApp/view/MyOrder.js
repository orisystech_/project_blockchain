import React, { Component } from 'react';
import { StyleSheet,FlatList, Image, View, ScrollView, Text, TouchableOpacity, Dimensions } from 'react-native';
import { StackNavigator } from 'react-navigation';
import {product} from '../components/entries.js'

//Constants
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
const cardHeight = screenHeight * 0.30;
const cardWidth = screenWidth * 0.95;

export default class MyOrder extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this._renderItem = this._renderItem.bind(this)
        this._keyExtractor = this._keyExtractor.bind(this)
        this.data = this.props.navigation.state.params.data
    }

    image(category,productName) {
        for (i=0;i<product.length;i++) {
            if (product[i].category==category) {
                let data = product[i].data
                for (j=0;j<data.length;j++) {
                    if (data[j].title == productName) {
                        return (
                            <Image source={data[j].illustration} style={styles.image_product}>
                            </Image>
                        );
                    }
                }
            }
        }
    }

    _renderItem({item}) { 
        return(
            <View style={styles.cardView}>
                <View style={{flexDirection: 'row'}} >
                    {this.image(item.Record["type"],item.Record["pname"])}
                    <View style={{flexDirection:'column', marginTop:15}}>
                        <View style={styles.lineStyle}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}>Order number: </Text>
                                <Text style={{fontSize: 12, fontWeight: 'bold', marginTop: 5}}>{item.Key}</Text>
                            </View>
                        </View>
                        <View style={styles.lineStyle}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}> Product Name: </Text>
                                <Text style={{fontSize: 18}}>{item.Record['pname']}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}> Owner(You): </Text>
                                <Text style={{fontSize: 18}}>{item.Record['owner']}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{fontSize: 18, fontWeight:'bold', color:'#666666', marginBottom: 10}}> Stato: </Text>
                                <Text style={{fontSize: 18}}>{item.Record['state']}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    _keyExtractor(item,index) {
        return item.Key
    }

    render() {
        console.log("data " + JSON.stringify(this.data))
        return (
            <ScrollView style={styles.container}>
                <View style={{alignItems:'center'}}>
                    <FlatList
                        data={this.data}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                    >
                    </FlatList>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: 'rgba(31,32,32,0.8)'
    },
    image_product: {
        resizeMode: 'center',
        height: 120,
        width: 120,
        marginLeft: 5,
        marginTop: 10,
        borderRadius: 4
    },
    cardView: {
        backgroundColor: '#cccccc',
        borderRadius: 8,
        height: cardHeight,
        width: cardWidth,
        marginBottom: 20
    },
    textDettagliOrdine: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 8,
        marginTop: 10
    },
    lineStyle: {
        borderColor: '#efefef',
        borderRadius: 3,
        borderBottomWidth: 2,
        borderRightWidth: 2, 
        marginLeft: 5,
        marginBottom: 10, 
        width: cardWidth * 0.60
    }
});

